#include "gl_hserver.hpp"

bool _hserver_status::_need_exit = false;
bool _hserver_status::_need_restart = false;

std::string _hserver_status::config_f_name;

std::string _hserver_status::main_server;
_net_cmd_client *_hserver_status::net_cc;

_session_manager *_hserver_status::sm;
_net_server *_hserver_status::http_s;

_db *_hserver_status::db;

_hserver_status::_state _hserver_status::s_state(0,"");
_locker _hserver_status::s_lock;

//_sys_stat_holder *_server_status::_sys_stat = NULL;

