#ifndef __CIBERVIEW_HTTP_SERVER_GLOBALS_H__
#define __CIBERVIEW_HTTP_SERVER_GLOBALS_H__

#include <obj_db5/base.hpp>
#include <obj_db5/base_db2.hpp>
#include <net_http_server.h>
#include <net_cmd_client.hpp>
#include <clthread.hpp>
#include "session.hpp"

class _hserver_status
{
    public:

      struct _state
      {
          int full_persent;
          std::string current_action;

          _state(int p,const std::string &s):full_persent(p),current_action(s) {};
          _state(int p,const char *s):full_persent(p),current_action(s) {};
      };

    private:

      static bool _need_exit;
      static bool _need_restart;

      static _net_cmd_client *net_cc;
      static std::string main_server;

      static std::string config_f_name;

      static _db *db;

      static _session_manager *sm;
      static _net_server *http_s;

      static _state s_state;
      static _locker s_lock;

    public:

      bool need_exit() { return _need_exit; };
      void need_exit(bool s) { _need_exit=s; };

      bool need_restart() { return _need_restart; };
      void need_restart(bool s) { _need_restart=s; };

      const std::string &main_server_name() const { return main_server; };
      void main_server_name(const std::string &s) { main_server = s; };

      _net_cmd_client *net_control() { return net_cc; };
      void net_control(_net_cmd_client *s) { net_cc=s; };

      _db *database() { return db; };
      void database(_db *s) { db=s; };

      _session_manager *session_manager() { return sm; };
      void session_manager(_session_manager *s) { sm=s; };

      _net_server *http_server() { return http_s; };
      void http_server(_net_server *s) { http_s=s; };

      const std::string &config_file_name() const { return config_f_name; };
      void config_file_name(const std::string &f) { config_f_name = f; };

//      const _state state() const { s_lock.lock(); _state res=s_state; s_lock.unlock(); return res; };
//      void state(const _state &s) { s_lock.lock(); s_state=s; s_lock.unlock(); };

};



#endif

