#include "http_calendar.hpp"
#include <net_http.h>
#include <htmlrep.hpp>
#include <time.hpp>
#include <stdlib.h>
#include <stdio.h>

// --------------------------------------
// --------------------------------------

// --------------------------------------
_http_calendar::_http_calendar(_client_session *session):_client_object(session,"calendar")
{
}

// --------------------------------------
_http_calendar::~_http_calendar()
{
}

// --------------------------------------
int _http_calendar::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez)
{
    int e_code=-1;
    
    _http_help *hh=create_http_help();
    _report *hr=create_html_report();
    std::string tmp;
    
    if       (act=="calendar.show")
    {
        hh->http_file(_session->root_dir(),"/comctl/calendar.html",rez);
        
        hr->setTemplateText(rez.c_str());
        std::string tmp;

        hr->setVar("form_name",hh->req_get_param(params,values,"form_name"));
        hr->setVar("day_f",hh->req_get_param(params,values,"day_f"));
        hr->setVar("month_f",hh->req_get_param(params,values,"month_f"));
        hr->setVar("year_f",hh->req_get_param(params,values,"year_f"));

        _time t;
        t.set_now();

        int i;
        
        i=atoi(hh->req_get_param(params,values,"d").c_str());
        if (i) t.day(i);
        i=atoi(hh->req_get_param(params,values,"m").c_str());
        if (i) t.month(i);
        i=atoi(hh->req_get_param(params,values,"y").c_str());
        if (i) t.year(i);

        hr->setVar("YEAR",t.year());
        hr->setVar("MONTH",t.month());

        hr->outSection("MAIN",rez);

        
        tm tm_set;
        tm_set.tm_year = t.year() - 1900;
        tm_set.tm_mon = t.month() - 1;
        tm_set.tm_mday = 1;
        tm_set.tm_hour  = 0;
        tm_set.tm_min   = 0;
        tm_set.tm_sec   = 0;
        tm_set.tm_isdst = 0;

        mktime(&tm_set);
    
        int first_day = tm_set.tm_wday;  // -1
        if (first_day==0) first_day=7;

        int days_in_month=31;
        if (t.month()==2) { if (t.year()%4==0) days_in_month=29; else days_in_month=28; }
        else if (t.month()==4 ||
                 t.month()==6 ||
                 t.month()==9 ||
                 t.month()==11) days_in_month=30;


        for(i=1;i<8;i++)
        {
            char st[16];
            sprintf(st,"D%i",i);
            hr->setVar(st,0);
        }
        
        for(i=0;i<days_in_month;i++)
        {
            char st[16];
            sprintf(st,"D%i",first_day);
            hr->setVar(st,i+1);
            sprintf(st,"PD%i",first_day);
            hr->setVar(st,i+1);

            sprintf(st,"FW%i",first_day);
            hr->setVar(st,"font-weight: normal;");
           
            first_day++;
            if (first_day==8)
            {
                first_day=1;
                hr->outSection("CROW",tmp);
                rez+=tmp;
            } 
        }

        if (first_day!=1)
        {
            for(i=first_day;i<8;i++)
            {
                char st[16];
                sprintf(st,"D%i",i);
                hr->setVar(st,0);
                sprintf(st,"PD%i",i);
                hr->setVar(st,"");
            }
            hr->outSection("CROW",tmp);
            rez+=tmp;
        }

        hr->outSection("END",tmp);
        rez+=tmp;
        
        e_code=0;
    }

    hh->release();
    hr->release();

    return e_code;
}

