#ifndef __HTTP_CALENDAR_OBJ_DB__
#define __HTTP_CALENDAR_OBJ_DB__

#include <string>
#include "../session.hpp"

// --------------------------------------------------
// --------------------------------------------------
class _http_calendar:public _client_object
{
    private:
    protected:
    public:

      _http_calendar(_client_session *session); 
      virtual ~_http_calendar();

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez);
};


#endif
