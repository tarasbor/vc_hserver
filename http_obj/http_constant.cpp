#include <udefs.hpp>
#include <net_http.h>
#include <htmlrep.hpp>
#ifdef VC_ORACLE
    #include <obj_db5/base_oracle.hpp>
#endif
#ifdef VC_DB2
    #include <obj_db5/base_db2.hpp>
#endif
#include "http_constant.hpp"


// --------------------------------------

_http_constant::_http_constant(_client_session* session) : _client_object(session, "constant")
{
}

_http_constant::~_http_constant()
{
}

int _http_constant::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    _db* db = NULL;
    _transaction* tr = NULL;
    _statement* st = NULL;

    _db_cl::_constant* c = NULL;

    if(act == "constant.new")
    {
        hh->http_file(_session->root_dir(), "/constant/constant.html", res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
        hr->outSection("MAIN", res);

        e_code = 0;
    }
    else if(act == "constant.show")
    {
        try
        {
            #ifdef VC_ORACLE
                db = create_db_oracle();
            #endif
            #ifdef VC_DB2
                db = create_db_db2();
            #endif
            tr = db->transaction();
            tr->start();

            c = new _db_cl::_constant(hh->req_get_param(params, values, "id"), tr);

            hh->http_file(_session->root_dir(), "/constant/constant.html", res);
            hr->setTemplateText(res.c_str());

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            hr->setVar("ID", c->id().value());
            hr->setVar("NAME", c->name());
            hr->setVar("VNAME", c->vname());
            hr->setVar("VALUE", c->cvalue());
            if(c->in_config())
                hr->setVar("IN_CONFIG", "checked");
            if(c->hide_value())
                hr->setVar("HIDE_VALUE", "checked");

            hr->outSection("MAIN", res);

            DEL(c);

            tr->commit();
            DEL(tr);
            DEL(db);
        }
        catch(_db_error& e)
        {
            if(c) delete c;
            if(st) { try { DEL(st); } catch(...) {} }
            if(tr) { try { tr->rollback(); DEL(tr); } catch(...) {} }
            if(db) { try { DEL(db); } catch(...) {} }
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "constant.apply")
    {
        try
        {
            #ifdef VC_ORACLE
                db = create_db_oracle();
            #endif
            #ifdef VC_DB2
                db = create_db_db2();
            #endif
            tr = db->transaction();
            tr->start();

            _db_id id(hh->req_get_param(params, values, "id"));
            c = id.is_set() ? new _db_cl::_constant(id, tr) : new _db_cl::_constant(tr);

            c->name(hh->req_get_param(params, values, "name"));
            c->vname(hh->req_get_param(params, values, "vname"));
            c->cvalue(hh->req_get_param(params, values, "value"));
            c->in_config(atoi(hh->req_get_param(params, values, "in_config").c_str()));
            c->hide_value(atoi(hh->req_get_param(params, values, "hide_value").c_str()));

            DEL(c);

            tr->commit();
            DEL(tr);
            DEL(db);

            hh->http_file(_session->root_dir(), "/comctl/done.html", res);
        }
        catch(_db_error& e)
        {
            if(c) delete c;
            if(st) { try { DEL(st); } catch(...) {} }
            if(tr) { try { tr->rollback(); DEL(tr); } catch(...) {} }
            if(db) { try { DEL(db); } catch(...) {} }
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "constant.delete")
    {
        hh->http_file(_session->root_dir(), "/constant/delete_constant.html", res);
        string tmp = string(res, res.find("\r\n\r\n"));
        hh->http_file(_session->root_dir(), "/comctl/yesno.html", res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
        hr->setVar("object_type", "constant");
        hr->setVar("action", "delete_commit");
        hr->setVar("id", hh->req_get_param(params, values, "id"));
        hr->setVarNoRep("question", tmp);

        hr->outSection("MAIN", res);
        e_code = 0;
    }
    else if(act == "constant.delete_commit")
    {
        try
        {
            _db_id id(hh->req_get_param(params, values, "id"));

            #ifdef VC_ORACLE
                db = create_db_oracle();
            #endif
            #ifdef VC_DB2
                db = create_db_db2();
            #endif

            tr = db->transaction();
            tr->start();
            st = tr->statement();

            st->prepare("delete from CONSTANTS where ID=:id<bigint>");
            st->push(id);
            st->execute();
            hh->http_file(_session->root_dir(), "/comctl/done.html", res);

            DEL(st);

            tr->commit();
            DEL(tr);
            DEL(db);
        }
        catch(_db_error& e)
        {
            if (st) { try { DEL(st); } catch(...) {} }
            if (tr) { try { tr->rollback(); DEL(tr); } catch(...) {} }
            if (db) { try { DEL(db); } catch(...) {} }
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

_http_constants_list::_http_constants_list(_client_session *session) : _client_object(session, "constants_list")
{
}

_http_constants_list::~_http_constants_list()
{
}

int _http_constants_list::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;
    std::string tmp;

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "constants_list.show")
    {
        try
        {
            hh->http_file(_session->root_dir(), "/constant/constants_list.html", res);
            hr->setTemplateText(res.c_str());

            hr->outSection("MAIN", res);

            _transaction *tr = db->transaction();
            tr->start();

            _db_cl::_constants_list::iterator* cit = _db_cl::_constants_list().begin(tr);
            int cnt;
            while(!cit->eof())
            {
                hr->setVar("LINE_MOD", cnt%2);
                hr->setVar("ID", (*cit)->id().value());
                hr->setVar("NAME", (*cit)->name());
                hr->setVar("VNAME", (*cit)->vname());
                if(!(*cit)->hide_value())
                    hr->setVar("VALUE", (*cit)->cvalue());
                else
                    hr->setVar("VALUE", "*******");

                if(!(*cit)->in_config())
                    hr->setVar("IN_CONFIG", "V");
                else
                    hr->setVar("IN_CONFIG", "X");

                hr->outSection("C_LINE",tmp);
                res += tmp;
                (*cit)++;
                cnt++;
            }
            delete cit;

            tr->commit();
            delete tr;

            hr->outSection("END", tmp);
            res += tmp;

        }
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}
