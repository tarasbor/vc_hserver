#ifndef __HTTP_OBJ_CONSTANT__
#define __HTTP_OBJ_CONSTANT__

#include <string>
#include "../common/db_obj/constant.hpp"
#include "../session.hpp"

// --------------------------------------------------
// --------------------------------------------------
class _http_constant:public _client_object
{
    private:
    protected:
    public:

      _http_constant(_client_session *session); 
      virtual ~_http_constant();

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez);
};

// --------------------------------------------------
// --------------------------------------------------
class _http_constants_list:public _client_object
{
    private:
    protected:
    public:

      _http_constants_list(_client_session *session); 
      virtual ~_http_constants_list();

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez);
};

#endif
