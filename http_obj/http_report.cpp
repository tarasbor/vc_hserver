#include "http_report.hpp"
#include <net_http.h>
#include <htmlrep.hpp>
#include <text_help.hpp>
#include <time.hpp>
#include <map>
#include <log.hpp>
#ifdef VC_ORACLE
#include <obj_db5/base_oracle.hpp>
#define CREATE_DB   create_db_oracle()
#endif
#ifdef VC_DB2
#include <obj_db5/base_db2.hpp>
#define CREATE_DB   create_db_db2()
#endif

#define MLOG(A) _log() << _log::_set_module_level("http_ui", (A))

using namespace std;

// --------------------------------------
enum _out_mode
{
    flat,
    by_opers,
    by_terms,
    by_queues
};

// --------------------------------------
struct _call_rec_report_full
{
    _time               begin_at;
    _time               answer_at;
    _time               end_at;
    int                 end_code;
    _db_id              call_id;
    _db_id              q_id;
    string              q_vname;
    _db_id              oper_id;
    string              oper_name;
    _db_id              term_id;
    string              term_vname;
    string              term_place;
    _time::time_val_i   wait_for;
    _time::time_val_i   serv_for;
};

// --------------------------------------
struct _grec_report_full
{
    string                  name;
    string                  info;

    int                     tcnt;

    int                     cnt_serv_ok;
    int                     cnt_serv_err;
    int                     cnt_no_serv_ok;
    int                     cnt_no_serv_err;

    _time::time_val_i       wait_for;
    _time::time_val_i       serv_for;
    list<_call_rec_report_full> calls;

    _grec_report_full() : name(""), info(""),
        tcnt(0), cnt_serv_ok(0), cnt_serv_err(0), cnt_no_serv_ok(0), cnt_no_serv_err(0),
        wait_for(0), serv_for(0) {}
};

// --------------------------------------
struct _drec_report_by_hours
{
    int             all_cnt;
    map<int,int>    hours;

    _drec_report_by_hours() : all_cnt(0) {}
};

// --------------------------------------
void make_hms_from_ts(_time::time_val_i dt, int& h, int& m, int& s)
{
    dt /= 1000;  // to s
    h = dt / 3600;
    m = (dt - h * 3600) / 60;
    s = (dt - h * 3600 - m * 60);
}

// --------------------------------------
struct _mfilter_rec
{
    _db_id  id;
    string  name;
    bool    is_selected;
};
typedef list<_mfilter_rec> _mfilter;

// --------------------------------------
void set_select_mfilter(_mfilter& filter, const string& ids_str)
{
    int index = 0;
    while(1)
    {
        _db_id id;
        id.from_str(_text_help::get_field_from_st(ids_str, ',', index));
        if(id.value() == 0)
            break;

        _mfilter::iterator it = filter.begin();
        while(it != filter.end())
        {
            if(id == it->id)
            {
                it->is_selected = true;
                break;
            }
            it++;
        }
        index++;
    }
}

// --------------------------------------
void make_array_mfilter(const string& arr_name, const _mfilter& filter, string& res)
{
    char tmp_s[256];
    char id_s[64];

    res = "";
    _mfilter::const_iterator it = filter.begin();
    bool one_selected = false;
    int index = 1;

    while(it != filter.end())
    {
        int sel = 0;
        if(it->is_selected)
        {
            sel = 1;
            one_selected = true;
        }
        sprintf(tmp_s, "%s[%i]= new selector('%s',%s,%i);\r",
                arr_name.c_str(), index, it->name.c_str(), it->id.value_str(id_s), sel);
        res += tmp_s;
        it++;
        index++;
    }
    res += one_selected ?
                arr_name + "[0]= new selector('...',0,0);\r" :
                arr_name + "[0]= new selector('...',0,1);\r";
}

// --------------------------------------
_http_report::_http_report(_client_session *session) : _client_object(session, "report")
{
}

// --------------------------------------
_http_report::~_http_report()
{
}

// --------------------------------------
int _http_report::cmd(std::string& act,list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    if      (act=="report.full") return report_full(params, values, res);
    else if (act=="report.call_info") return report_call_info(params, values, res);
    else if (act=="report.sum") return report_sum(params, values, res);

    return -1;
}

// --------------------------------------
int _http_report::report_full(list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    std::string tmp;
    char tmp_s[1024];

    _http_help *hh = create_http_help();
    _report *hr = create_html_report();

    _db *db=NULL;
    _transaction *tr=NULL;
    _statement *st=NULL;

    try
    {
        db=CREATE_DB;

        tr = db->transaction();
        tr->start();

        // load template
        if(hh->req_get_param(params, values, "export") == "1")
        {
            hr->setTemplate((_session->root_dir() + "/reports/report_full_export.html").c_str());
        }
        else
        {
            hh->http_file(_session->root_dir(), "/reports/report_full.html", res);
            hr->setTemplateText(res.c_str());
        }

        // parse filters
        _time begin_t, end_t;

        if(hh->req_get_param(params, values, "begin_d") != "")
        {
            begin_t = _time(atoi(hh->req_get_param(params, values, "begin_y").c_str()),
                            atoi(hh->req_get_param(params, values, "begin_m").c_str()),
                            atoi(hh->req_get_param(params, values, "begin_d").c_str()),
                            0, 0, 0, 0);
        }
        else
            begin_t.set_now();

        hr->setVar("BEGIN_D", begin_t.day());
        hr->setVar("BEGIN_M", begin_t.month());
        hr->setVar("BEGIN_Y", begin_t.year());

        if(hh->req_get_param(params, values, "end_d") != "")
        {
            end_t = _time(atoi(hh->req_get_param(params, values, "end_y").c_str()),
                          atoi(hh->req_get_param(params, values, "end_m").c_str()),
                          atoi(hh->req_get_param(params, values, "end_d").c_str()),
                          23,59,59,999);
        }
        else
        {
            end_t = begin_t;
            end_t.tick(24 * 60 * 60 * 1000 - 1);
        }

        hr->setVar("END_D", end_t.day());
        hr->setVar("END_M", end_t.month());
        hr->setVar("END_Y", end_t.year());

        if(hh->req_get_param(params, values, "begin_h") != "")
        {
            begin_t.hour(atoi(hh->req_get_param(params, values, "begin_h").c_str()));
            hr->setVar("BEGIN_H", begin_t.hour());

            if(hh->req_get_param(params, values, "begin_n") != "")
            {
                begin_t.minute(atoi(hh->req_get_param(params, values, "begin_n").c_str()));
                hr->setVar("BEGIN_N", begin_t.minute());
            }
        }

        if(hh->req_get_param(params,values,"end_h") != "")
        {
            end_t.hour(atoi(hh->req_get_param(params, values, "end_h").c_str()));
            hr->setVar("END_H", end_t.hour());

            if(hh->req_get_param(params, values, "end_n") != "")
            {
                end_t.minute(atoi(hh->req_get_param(params, values, "end_n").c_str()));
                hr->setVar("END_N", end_t.minute());
            }
            else
            {
                end_t.minute(0);
                hr->setVar("END_N", 0);
            }
        }

        //

        st = tr->statement();

        string sel_arr;

        // operator filter
        _mfilter oper_filter;
        st->prepare("select ID,FIO_F,FIO_I,FIO_O from VCS.GL_USERS");
        st->execute();
        string uname_i, uname_o;
        while(st->fetch())
        {
            _mfilter_rec r;

            st->pop(r.id);
            st->pop(r.name);
            st->pop(uname_i);
            st->pop(uname_o);
            if(uname_i.length())
            {
                r.name += " ";
                r.name += uname_i;
            }
            r.is_selected = false;

            oper_filter.push_back(r);
        }
        string oper_sel_ids_str = hh->req_get_param(params, values, "oper_sel_ids");
        if(oper_sel_ids_str == "0")
            oper_sel_ids_str = "";
        set_select_mfilter(oper_filter, oper_sel_ids_str);
        make_array_mfilter("oper_sel_arr", oper_filter, sel_arr);
        hr->setVar("OPER_SEL_ARRAY", sel_arr);

        // terminal filter
        _mfilter term_filter;
        st->prepare("select ID,VNAME,PLACE from VCS.TERMS");//DNS_NAME?
        st->execute();
        string term_place;
        while(st->fetch())
        {
            _mfilter_rec r;
            st->pop(r.id);
            st->pop(r.name);
            st->pop(term_place);
            if(term_place.length())
            {
                r.name += " (";
                r.name += term_place;
                r.name += ")";
            }
            r.is_selected = false;

            term_filter.push_back(r);
        }
        string term_sel_ids_str = hh->req_get_param(params, values, "term_sel_ids");
        if(term_sel_ids_str == "0")
            term_sel_ids_str = "";
        set_select_mfilter(term_filter, term_sel_ids_str);
        make_array_mfilter("term_sel_arr", term_filter, sel_arr);
        hr->setVar("TERM_SEL_ARRAY", sel_arr);

        // queue filter
        _mfilter queue_filter;
        st->prepare("select ID,VNAME from VCS.QSERVER_QS");
        st->execute();
        while(st->fetch())
        {
            _mfilter_rec r;
            st->pop(r.id);
            st->pop(r.name);
            r.is_selected = false;

            queue_filter.push_back(r);
        }
        string queue_sel_ids_str = hh->req_get_param(params, values, "queue_sel_ids");
        if(queue_sel_ids_str == "0")
            queue_sel_ids_str = "";
        set_select_mfilter(queue_filter, queue_sel_ids_str);
        make_array_mfilter("queue_sel_arr", queue_filter, sel_arr);
        hr->setVar("QUEUE_SEL_ARRAY", sel_arr);

        // waiting time filter
        int wait_time_search = atoi(hh->req_get_param(params, values, "wait_time_search").c_str());
        if(wait_time_search)
            hr->setVar("WAIT_TIME_SEARCH", wait_time_search);
        string wait_time_cond = hh->req_get_param(params, values, "wait_time_cond");
        hr->setVar((string("WAIT_TIME_COND") + wait_time_cond).c_str(), "selected");

        // service time filter
        int serv_time_search = atoi(hh->req_get_param(params, values, "serv_time_search").c_str());
        if(serv_time_search)
            hr->setVar("SERV_TIME_SEARCH", serv_time_search);
        string serv_time_cond = hh->req_get_param(params, values, "serv_time_cond");
        hr->setVar((string("SERV_TIME_COND") + serv_time_cond).c_str(), "selected");

        // call end code filter
        string end_code_cond = hh->req_get_param(params, values, "end_code_cond");
        hr->setVar((string("END_CODE_COND") + end_code_cond).c_str(), "selected");

        // statistics mode (grouping)
        _out_mode out_mode;
        string out_mode_str = hh->req_get_param(params, values, "out_mode");
        if (out_mode_str.empty()) out_mode_str="flat";

        if      (out_mode_str == "flat") out_mode = flat;
        else if (out_mode_str == "by_opers") out_mode = by_opers;
        else if (out_mode_str == "by_terms") out_mode = by_terms;
        else if (out_mode_str == "by_queues") out_mode = by_queues;
        hr->setVar((string("OUT_MODE_") + out_mode_str).c_str(), "checked");

        bool hide_calls = hh->req_get_param(params,values,"hide_calls") != "";
        if(hide_calls) hr->setVar("HIDE_CALLS", "checked");

        // out report header
        hr->outSection("MAIN", res);

        // set filters
        if(hh->req_get_param(params, values, "begin_d") != "")
        {
            _selector sel;

            sel.add_var("C.BEGIN_AT");
            sel.add_var("C.ANSWER_AT");
            sel.add_var("C.END_AT");
            sel.add_var("C.END_CODE");
            sel.add_var("C.ID");
            sel.add_var("Q.ID");
            sel.add_var("Q.VNAME");
            sel.add_var("U.ID");
            sel.add_var("U.FIO_F");
            sel.add_var("U.FIO_I");
            sel.add_var("U.FIO_O");
            sel.add_var("T.ID");
            sel.add_var("T.VNAME");
            sel.add_var("T.PLACE");

            sel.add_from("VCS.CALLS C");
            sel.add_join("inner join VCS.QSERVER_QS Q on C.Q_ID=Q.ID");
            sel.add_join("inner join VCS.TERMS T on C.TERM_ID=T.ID");
            sel.add_join("left join VCS.GL_USERS U on C.OPER_ID=U.ID");

            sel.add_cond("C.BEGIN_AT > :bs<bigint>");
            sel.add_cond("C.BEGIN_AT < :es<bigint>");
            sel.add_cond("U.ID is not null");   // Fix proshin base_db2.cpp pop() bug. Don`t show calls with oper_id==NULL

            if(oper_sel_ids_str != "")
                sel.add_cond(string("U.ID in (") + oper_sel_ids_str + ")");
            if(term_sel_ids_str != "")
                sel.add_cond(string("T.ID in (") + term_sel_ids_str + ")");
            if(queue_sel_ids_str != "")
                sel.add_cond(string("Q.ID in (") + queue_sel_ids_str + ")");

            sel.set_order("C.BEGIN_AT");

            if(!end_code_cond.empty())
            {
                if		(end_code_cond == "proc")  sel.add_cond("C.END_CODE=0");
                else if (end_code_cond == "no_proc") sel.add_cond("C.END_CODE=1");
                else if (end_code_cond == "crush") sel.add_cond("C.END_CODE=2");
            }

            if(wait_time_search)
            {
                _time::time_val_i wts = wait_time_search;
                wts *= 60 * 1000 * 10000;
                char op;
                if(wait_time_cond == "more") op = '>';
                else if(wait_time_cond == "less") op = '<';
                else if(wait_time_cond == "eq") op = '=';
                sprintf(tmp_s, "C.ANSWER_AT-C.BEGIN_AT %c %Li", op, wts);
                sel.add_cond(tmp_s);
            }

            if(serv_time_search)
            {
                _time::time_val_i sts = serv_time_search;
                sts *= 60 * 1000 * 10000;
                char op;
                if(serv_time_cond == "more") op = '>';
                else if(serv_time_cond == "less") op = '<';
                else if(serv_time_cond == "eq") op = '=';
                sprintf(tmp_s, "C.END_AT-C.ANSWER_AT %c %Li", op, sts);
                sel.add_cond(tmp_s);
            }

            sel.set_order("C.BEGIN_AT");

            st->prepare(sel.sql_ref());
            st->push(begin_t);
            st->push(end_t);
            st->execute();

//            MLOG(0) << sel.sql_ref() << endl;   // // fix bug calls with oper_id==NULL

            _grec_report_full gall;
            map<_db_id, _grec_report_full> groups;
            _db_id gvalue(0);

            _time prev_begin_at = 0;
            _time::time_val_i prev_wait_for = 0;

            while(st->fetch())
            {
                _call_rec_report_full c;
//                uname_i.clear(); uname_o.clear();   // fix bug calls with oper_id==NULL

                st->pop(c.begin_at);
                st->pop(c.answer_at);
                st->pop(c.end_at);
                st->pop(&c.end_code);
                st->pop(c.call_id);
                st->pop(c.q_id);
                st->pop(c.q_vname);
                st->pop(c.oper_id);     // BUG NULL value replaced with other value
                st->pop(c.oper_name);   // BUG NULL value replaced with other value
                st->pop(uname_i);
                st->pop(uname_o);
                if(uname_i.length())
                {
                    c.oper_name += " ";
                    c.oper_name += uname_i;
                }
                st->pop(c.term_id);
                st->pop(c.term_vname);
                st->pop(c.term_place);

                c.serv_for=0;
                if (c.answer_at.stamp())
                {
                    c.wait_for=c.answer_at.stamp() - c.begin_at.stamp();
                    c.wait_for /= 10000;

                    c.serv_for = c.end_at.stamp() - c.answer_at.stamp();
                    c.serv_for /= 10000;
                }
                else
                {
                    c.wait_for=c.end_at.stamp() - c.begin_at.stamp();
                    c.wait_for /= 10000;
                }

//                MLOG(0) <<  c.call_id.value() << " oper_name(" << c.oper_name << ") oper_id(" << c.oper_id.value() << ")" << endl;

                // select grouped param & its value (by out mode)
                // if out mode = flat - we will have only one group
                if      (out_mode==by_opers) gvalue=c.oper_id;
                else if (out_mode==by_terms) gvalue=c.term_id;
                else if (out_mode==by_queues) gvalue=c.q_id;

                // find (or insert) record about this group in map
                map<_db_id, _grec_report_full>::iterator git = groups.find(gvalue);
                if(git == groups.end())
                {
                    git = groups.insert(pair<_db_id, _grec_report_full>(gvalue, _grec_report_full())).first;

                    if      (out_mode==by_opers) git->second.name = c.oper_name;
                    else if (out_mode==by_terms)
                    {
                        git->second.name = c.term_vname;
                        git->second.info = c.term_place;
                    }
                    else if (out_mode==by_queues) git->second.name = c.q_vname;

                    git->second.tcnt = 0;
                    git->second.cnt_serv_ok=0;
                    git->second.cnt_serv_err=0;
                    git->second.cnt_no_serv_ok=0;
                    git->second.cnt_no_serv_err=0;

                    git->second.serv_for = 0;
                    git->second.wait_for = 0;
                }

                // & update it
                git->second.serv_for += c.serv_for;
                git->second.wait_for += c.wait_for;
                git->second.tcnt++;

                if (c.end_code==0)      git->second.cnt_serv_ok++;
                else if (c.end_code==1) git->second.cnt_no_serv_ok++;
                else if (c.end_code==2)
                {
                    if (c.serv_for) git->second.cnt_serv_err++; else git->second.cnt_no_serv_err++;
                }

                // update global stat
                gall.serv_for += c.serv_for;
                gall.wait_for += c.wait_for;
                gall.tcnt++;

                if (c.end_code==0)      gall.cnt_serv_ok++;
                else if (c.end_code==1) gall.cnt_no_serv_ok++;
                else if (c.end_code==2)
                {
                    if (c.serv_for) gall.cnt_serv_err++; else gall.cnt_no_serv_err++;
                }

                if(!hide_calls)
                    git->second.calls.push_back(c);
            }
            // end of fetching

            // begin out data to html
            map<_db_id, _grec_report_full>::iterator git = groups.begin();
            int h, m, s;

            // out qs or opers begin section
            if(hide_calls)
            {
                if      (out_mode==by_opers) hr->outSection("GOPER_BEGIN", tmp);
                else if (out_mode==by_terms) hr->outSection("GTERM_BEGIN", tmp);
                else if (out_mode==by_queues) hr->outSection("GQ_BEGIN", tmp);
                else tmp.clear();
            }
            else
            {
                tmp.clear();
            }
            res+=tmp;

            while(git != groups.end())
            {
                hr->setVar("NAME", git->second.name);
                hr->setVar("INFO", git->second.info);

                if(git->second.tcnt)
                {
                    hr->setVar("COUNT", git->second.tcnt);
                    hr->setVar("COUNT_SERV_OK", git->second.cnt_serv_ok);
                    hr->setVar("COUNT_SERV_ERR", git->second.cnt_serv_err);
                    hr->setVar("COUNT_NO_SERV_OK", git->second.cnt_no_serv_ok);
                    hr->setVar("COUNT_NO_SERV_ERR", git->second.cnt_no_serv_err);
                    hr->setVar("COUNT_ERR", git->second.cnt_no_serv_err+git->second.cnt_serv_err);

                    make_hms_from_ts(git->second.wait_for, h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("SUM_WAIT_TIME", tmp_s);

                    make_hms_from_ts(git->second.wait_for / git->second.tcnt, h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("AVG_WAIT_TIME", tmp_s);

                    make_hms_from_ts(git->second.serv_for, h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("SUM_SERV_TIME", tmp_s);

                    if (git->second.cnt_serv_ok + git->second.cnt_serv_err)
                    {
                        make_hms_from_ts(git->second.serv_for / (git->second.cnt_serv_ok + git->second.cnt_serv_err), h, m, s);
                        sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                        hr->setVar("AVG_SERV_TIME", tmp_s);
                    }
                    else
                    {
                        hr->setVar("AVG_SERV_TIME","-");
                    }
                }
                else
                {
                    hr->setVar("COUNT", 0);
                    hr->setVar("COUNT_SERV_OK", 0);
                    hr->setVar("COUNT_SERV_ERR", 0);
                    hr->setVar("COUNT_NO_SERV_OK", 0);
                    hr->setVar("COUNT_NO_SERV_ERR", 0);
                    hr->setVar("COUNT_ERR", 0);
                    hr->setVar("SUM_WAIT_TIME", "-");
                    hr->setVar("SUM_SERV_TIME", "-");
                    hr->setVar("AVG_WAIT_TIME", "-");
                    hr->setVar("AVG_SERV_TIME", "-");
                }

                if (hide_calls)
                {
                    if      (out_mode==by_opers) hr->outSection("GOPER_LINE", tmp);
                    else if (out_mode==by_terms) hr->outSection("GTERM_LINE", tmp);
                    else if (out_mode==by_queues) hr->outSection("GQ_LINE", tmp);
                    else tmp.clear();
                    res += tmp;
                }
                else
                {
                    if      (out_mode==by_opers) hr->outSection("GOPERT_LINE", tmp);
                    else if (out_mode==by_terms) hr->outSection("GTERMT_LINE", tmp);
                    else if (out_mode==by_queues) hr->outSection("GQT_LINE", tmp);
                    else tmp.clear();
                    res += tmp;

                    hr->outSection("CALLS_BEGIN", tmp);
                    res += tmp;

                    list<_call_rec_report_full>::iterator call_it = git->second.calls.begin();
                    while(call_it != git->second.calls.end())
                    {
                        hr->setVar("CALL_NUM", call_it->call_id.value_str(tmp_s));

                        hr->setVar("OPER_FIO", call_it->oper_name);

                        hr->setVar("TERM_VNAME_PLACE", call_it->term_place.empty() ?
                                       call_it->term_vname : call_it->term_vname + " (" + call_it->term_place + ")");

                        hr->setVar("QUEUE_VNAME", call_it->q_vname);

                        sprintf(tmp_s, "%04i.%02i.%02i %02i:%02i:%02i ",
                                call_it->begin_at.year(), call_it->begin_at.month(), call_it->begin_at.day(),
                                call_it->begin_at.hour(), call_it->begin_at.minute(), call_it->begin_at.second());
                        hr->setVar("BEGIN_AT", tmp_s);

                        if (call_it->answer_at.stamp())
                        {
                            sprintf(tmp_s, "%04i.%02i.%02i %02i:%02i:%02i ",
                                    call_it->answer_at.year(), call_it->answer_at.month(), call_it->answer_at.day(),
                                    call_it->answer_at.hour(), call_it->answer_at.minute(), call_it->answer_at.second());
                            hr->setVar("ANSWERED_AT", tmp_s);
                        }
                        else
                        {
                            hr->setVar("ANSWERED_AT", "-");
                        }


                        sprintf(tmp_s, "%04i.%02i.%02i %02i:%02i:%02i ",
                                call_it->end_at.year(), call_it->end_at.month(), call_it->end_at.day(),
                                call_it->end_at.hour(), call_it->end_at.minute(), call_it->end_at.second());
                        hr->setVar("END_AT", tmp_s);

                        make_hms_from_ts(call_it->wait_for, h, m, s);
                        sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                        hr->setVar("WAIT_TIME", tmp_s);

                        if (call_it->answer_at.stamp())
                        {
                            make_hms_from_ts(call_it->serv_for, h, m, s);
                            sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                            hr->setVar("SERV_TIME", tmp_s);
                        }
                        else
                        {
                            hr->setVar("SERV_TIME", "-");
                        }

                        if      (call_it->end_code==0) hr->setVar("END_CODE", "Обслужен");
                        else if (call_it->end_code==1) hr->setVar("END_CODE", "Не обслужен");
                        else if (call_it->end_code==2) hr->setVar("END_CODE", "Обрыв связи");
                        else hr->setVar("END_CODE", "Закрыт системой");

                        hr->outSection("CALLS_LINE", tmp);
                        res += tmp;

                        call_it++;
                    }

                    hr->outSection("CALLS_END",tmp); // calls table footer
                    res += tmp;
                }
                git++;
            }

            if(gall.tcnt)
            {
                hr->setVar("COUNT", gall.tcnt);
                hr->setVar("COUNT_SERV_OK", gall.cnt_serv_ok);
                hr->setVar("COUNT_SERV_ERR", gall.cnt_serv_err);
                hr->setVar("COUNT_NO_SERV_OK", gall.cnt_no_serv_ok);
                hr->setVar("COUNT_NO_SERV_ERR", gall.cnt_no_serv_err);
                hr->setVar("COUNT_ERR", gall.cnt_no_serv_err+gall.cnt_serv_err);

                make_hms_from_ts(gall.wait_for, h, m, s);
                sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                hr->setVar("SUM_WAIT_TIME", tmp_s);

                make_hms_from_ts(gall.wait_for / gall.tcnt, h, m, s);
                sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                hr->setVar("AVG_WAIT_TIME", tmp_s);

                make_hms_from_ts(gall.serv_for, h, m, s);
                sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                hr->setVar("SUM_SERV_TIME", tmp_s);

                if (gall.cnt_serv_ok + gall.cnt_serv_err)
                {
                    make_hms_from_ts(gall.serv_for / (gall.cnt_serv_ok + gall.cnt_serv_err), h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("AVG_SERV_TIME", tmp_s);
                }
                else
                {
                    hr->setVar("AVG_SERV_TIME","-");
                }
            }
            else
            {
                hr->setVar("COUNT", 0);
                hr->setVar("COUNT_SERV_OK", 0);
                hr->setVar("COUNT_SERV_ERR", 0);
                hr->setVar("COUNT_NO_SERV_OK", 0);
                hr->setVar("COUNT_NO_SERV_ERR", 0);
                hr->setVar("COUNT_ERR", 0);
                hr->setVar("SUM_WAIT_TIME", "-");
                hr->setVar("SUM_SERV_TIME", "-");
                hr->setVar("AVG_WAIT_TIME", "-");
                hr->setVar("AVG_SERV_TIME", "-");
            }

            if(hide_calls)
            {
                if      (out_mode==by_opers) hr->outSection("GOPER_END", tmp);
                else if (out_mode==by_terms) hr->outSection("GTERM_END", tmp);
                else if (out_mode==by_queues) hr->outSection("GQ_END", tmp);
                else hr->outSection("SUM", tmp);
            }
            else
            {
                hr->outSection("SUM", tmp);
            }
            res += tmp;
        }

        delete st;
        st = NULL;

        tr->commit();
        delete tr;
        tr = NULL;

        delete db;
        db = NULL;

        hr->outSection("END", tmp); // report footer
        res += tmp;

        if(hh->req_get_param(params, values, "export") == "1")
        {
            char fname[256];
            sprintf(fname, "%04i_%02i_%02i__%04i_%02i_%02i.xls",
                    begin_t.year(), begin_t.month(), begin_t.day(),
                    end_t.year(), end_t.month(), end_t.day());

            ofstream f_tmp;
            f_tmp.open((_session->root_dir() + "tmp/report_full_" + fname).c_str(), ios::out | ios::binary);
            f_tmp.write(res.data(), res.length());
            f_tmp.close();

            hh->http_file(_session->root_dir(), "/comctl/redir_to_file.html", res);
            hr->setTemplateText(res.c_str());
            hr->setVar("FILENAME", string("/tmp/report_full_") + fname);
            hr->outSection("MAIN", res);
        }
    }
    catch(_db_error& e)
    {
        if(st) { try { delete st; } catch(...) {} }
        if(tr) { try { tr->rollback(); delete tr; } catch(...) {} }
        if(db) { try { delete db; } catch(...) {} }
        _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
    }

    e_code = 0;

    hh->release();
    hr->release();

    return e_code;
}

// ------------------------------------------------
int _http_report::report_call_info(list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    _db* db = NULL;
    _transaction* tr = NULL;
    _statement* st = NULL;

    try
    {
        db = CREATE_DB;

        tr = db->transaction();
        tr->start();

        string tmp;

        // LOAD TEMPLATE
        if(hh->req_get_param(params, values, "export") == "1")
        {
            hr->setTemplate((_session->root_dir() + "reports/report_call_info_export.html").c_str());
        }
        else
        {
            hh->http_file(_session->root_dir(), "/reports/report_call_info.html", res);
            hr->setTemplateText(res.c_str());
        }


        tr->commit();
        delete tr;
        tr = NULL;

        delete db;
        db = NULL;

        hr->outSection("END", tmp);
        res += tmp;

        if(hh->req_get_param(params, values, "export") == "1")
        {
            string fname="1.xls";
            /* TODO - make fname from call id*/

            ofstream f_tmp;
            f_tmp.open((_session->root_dir() + "tmp/report_call_info_" + fname).c_str(), ios::out | ios::binary);
            f_tmp.write(res.data(), res.length());
            f_tmp.close();

            hh->http_file(_session->root_dir(), "/comctl/redir_to_file.html", res);
            hr->setTemplateText(res.c_str());
            hr->setVar("FILENAME", string("/tmp/report_call_info_") + fname);
            hr->outSection("MAIN", res);
        }
    }
    catch(_db_error& e)
    {
        if(st) { try { delete st; } catch(...) {} }
        if(tr) { try { tr->rollback(); delete tr; } catch(...) {} }
        if(db) { try { delete db; } catch(...) {} }
        _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
    }

    e_code = 0;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------
int _http_report::report_sum(list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    std::string tmp;
    char tmp_s[1024];

    _http_help *hh = create_http_help();
    _report *hr = create_html_report();

    _db *db=NULL;
    _transaction *tr=NULL;
    _statement *st=NULL;

    try
    {
        db=CREATE_DB;

        tr = db->transaction();
        tr->start();

        // load template
        if(hh->req_get_param(params, values, "export") == "1")
        {
            hr->setTemplate((_session->root_dir() + "/reports/report_sum_export.html").c_str());
        }
        else
        {
            hh->http_file(_session->root_dir(), "/reports/report_sum.html", res);
            hr->setTemplateText(res.c_str());
        }

        // parse filters
        _time begin_t, end_t;

        if(hh->req_get_param(params, values, "begin_d") != "")
        {
            begin_t = _time(atoi(hh->req_get_param(params, values, "begin_y").c_str()),
                            atoi(hh->req_get_param(params, values, "begin_m").c_str()),
                            atoi(hh->req_get_param(params, values, "begin_d").c_str()),
                            0, 0, 0, 0);
        }
        else
            begin_t.set_now();

        hr->setVar("BEGIN_D", begin_t.day());
        hr->setVar("BEGIN_M", begin_t.month());
        hr->setVar("BEGIN_Y", begin_t.year());

        if(hh->req_get_param(params, values, "end_d") != "")
        {
            end_t = _time(atoi(hh->req_get_param(params, values, "end_y").c_str()),
                          atoi(hh->req_get_param(params, values, "end_m").c_str()),
                          atoi(hh->req_get_param(params, values, "end_d").c_str()),
                          23,59,59,999);
        }
        else
        {
            end_t = begin_t;
            end_t.tick(24 * 60 * 60 * 1000 - 1);
        }

        hr->setVar("END_D", end_t.day());
        hr->setVar("END_M", end_t.month());
        hr->setVar("END_Y", end_t.year());

        if(hh->req_get_param(params, values, "begin_h") != "")
        {
            begin_t.hour(atoi(hh->req_get_param(params, values, "begin_h").c_str()));
            hr->setVar("BEGIN_H", begin_t.hour());

            if(hh->req_get_param(params, values, "begin_n") != "")
            {
                begin_t.minute(atoi(hh->req_get_param(params, values, "begin_n").c_str()));
                hr->setVar("BEGIN_N", begin_t.minute());
            }
        }

        if(hh->req_get_param(params,values,"end_h") != "")
        {
            end_t.hour(atoi(hh->req_get_param(params, values, "end_h").c_str()));
            hr->setVar("END_H", end_t.hour());

            if(hh->req_get_param(params, values, "end_n") != "")
            {
                end_t.minute(atoi(hh->req_get_param(params, values, "end_n").c_str()));
                hr->setVar("END_N", end_t.minute());
            }
            else
            {
                end_t.minute(0);
                hr->setVar("END_N", 0);
            }
        }

        st = tr->statement();

        string sel_arr;

        // operator filter
        _mfilter oper_filter;
        st->prepare("select ID,FIO_F,FIO_I,FIO_O from VCS.GL_USERS");
        st->execute();
        string uname_i, uname_o;
        while(st->fetch())
        {
            _mfilter_rec r;

            st->pop(r.id);
            st->pop(r.name);
            st->pop(uname_i);
            st->pop(uname_o);
            if(uname_i.length())
            {
                r.name += " ";
                r.name += uname_i;
            }
            r.is_selected = false;

            oper_filter.push_back(r);
        }
        string oper_sel_ids_str = hh->req_get_param(params, values, "oper_sel_ids");
        if(oper_sel_ids_str == "0")
            oper_sel_ids_str = "";
        set_select_mfilter(oper_filter, oper_sel_ids_str);
        make_array_mfilter("oper_sel_arr", oper_filter, sel_arr);
        hr->setVar("OPER_SEL_ARRAY", sel_arr);

        // terminal filter
        _mfilter term_filter;
        st->prepare("select ID,VNAME,PLACE from VCS.TERMS");//DNS_NAME?
        st->execute();
        string term_place;
        while(st->fetch())
        {
            _mfilter_rec r;
            st->pop(r.id);
            st->pop(r.name);
            st->pop(term_place);
            if(term_place.length())
            {
                r.name += " (";
                r.name += term_place;
                r.name += ")";
            }
            r.is_selected = false;

            term_filter.push_back(r);
        }
        string term_sel_ids_str = hh->req_get_param(params, values, "term_sel_ids");
        if(term_sel_ids_str == "0")
            term_sel_ids_str = "";
        set_select_mfilter(term_filter, term_sel_ids_str);
        make_array_mfilter("term_sel_arr", term_filter, sel_arr);
        hr->setVar("TERM_SEL_ARRAY", sel_arr);

        // queue filter
        _mfilter queue_filter;
        st->prepare("select ID,VNAME from VCS.QSERVER_QS");
        st->execute();
        while(st->fetch())
        {
            _mfilter_rec r;
            st->pop(r.id);
            st->pop(r.name);
            r.is_selected = false;

            queue_filter.push_back(r);
        }
        string queue_sel_ids_str = hh->req_get_param(params, values, "queue_sel_ids");
        if(queue_sel_ids_str == "0")
            queue_sel_ids_str = "";
        set_select_mfilter(queue_filter, queue_sel_ids_str);
        make_array_mfilter("queue_sel_arr", queue_filter, sel_arr);
        hr->setVar("QUEUE_SEL_ARRAY", sel_arr);

        // statistics mode (grouping)
        _out_mode out_mode;
        string out_mode_str = hh->req_get_param(params, values, "out_mode");
        if (out_mode_str.empty()) out_mode_str="by_opers";

        if      (out_mode_str == "by_opers") out_mode = by_opers;
        else if (out_mode_str == "by_terms") out_mode = by_terms;
        else if (out_mode_str == "by_queues") out_mode = by_queues;
        hr->setVar((string("OUT_MODE_") + out_mode_str).c_str(), "checked");

        // out report header
        hr->outSection("MAIN", res);

        // set filters
        if(hh->req_get_param(params, values, "begin_d") != "")
        {
            _selector sel;

            sel.add_var("C.BEGIN_AT");
            sel.add_var("C.ANSWER_AT");
            sel.add_var("C.END_AT");
            sel.add_var("C.END_CODE");
            sel.add_var("C.ID");
            sel.add_var("Q.ID");
            sel.add_var("Q.VNAME");
            sel.add_var("U.ID");
            sel.add_var("U.FIO_F");
            sel.add_var("U.FIO_I");
            sel.add_var("U.FIO_O");
            sel.add_var("T.ID");
            sel.add_var("T.VNAME");
            sel.add_var("T.PLACE");

            sel.add_from("VCS.CALLS C");
            sel.add_join("inner join VCS.QSERVER_QS Q on C.Q_ID=Q.ID");
            sel.add_join("inner join VCS.TERMS T on C.TERM_ID=T.ID");
            sel.add_join("left join VCS.GL_USERS U on C.OPER_ID=U.ID");

            sel.add_cond("C.BEGIN_AT > :bs<bigint>");
            sel.add_cond("C.BEGIN_AT < :es<bigint>");
            sel.add_cond("U.ID is not null");   // Fix proshin base_db2.cpp pop() bug. Don`t show calls with oper_id==NULL

            if(oper_sel_ids_str != "")
                sel.add_cond(string("U.ID in (") + oper_sel_ids_str + ")");
            if(term_sel_ids_str != "")
                sel.add_cond(string("T.ID in (") + term_sel_ids_str + ")");
            if(queue_sel_ids_str != "")
                sel.add_cond(string("Q.ID in (") + queue_sel_ids_str + ")");

            if      (out_mode_str == "by_opers")  sel.set_order("U.FIO_F, U.FIO_I, U.FIO_O");
            else if (out_mode_str == "by_terms")  sel.set_order("T.VNAME");
            else if (out_mode_str == "by_queues") sel.set_order("Q.VNAME");

            st->prepare(sel.sql_ref());
            st->push(begin_t);
            st->push(end_t);
            st->execute();

            _grec_report_full gall;
            map<_db_id, _grec_report_full> groups;
            _db_id gvalue(0);

            _time prev_begin_at = 0;
            _time::time_val_i prev_wait_for = 0;

            while(st->fetch())
            {
                _call_rec_report_full c;

                st->pop(c.begin_at);
                st->pop(c.answer_at);
                st->pop(c.end_at);
                st->pop(&c.end_code);
                st->pop(c.call_id);
                st->pop(c.q_id);
                st->pop(c.q_vname);
                st->pop(c.oper_id);
                st->pop(c.oper_name);
                st->pop(uname_i);
                st->pop(uname_o);
                if(uname_i.length())
                {
                    c.oper_name += " ";
                    c.oper_name += uname_i;
                }
                st->pop(c.term_id);
                st->pop(c.term_vname);
                st->pop(c.term_place);

                c.serv_for=0;
                if (c.answer_at.stamp())
                {
                    c.wait_for=c.answer_at.stamp() - c.begin_at.stamp();
                    c.wait_for /= 10000;

                    c.serv_for = c.end_at.stamp() - c.answer_at.stamp();
                    c.serv_for /= 10000;
                }
                else
                {
                    c.wait_for=c.end_at.stamp() - c.begin_at.stamp();
                    c.wait_for /= 10000;
                }

                // select grouped param & its value (by out mode)
                // if out mode = flat - we will have only one group
                if      (out_mode==by_opers) gvalue=c.oper_id;
                else if (out_mode==by_terms) gvalue=c.term_id;
                else if (out_mode==by_queues) gvalue=c.q_id;

                // find (or insert) record about this group in map
                map<_db_id, _grec_report_full>::iterator git = groups.find(gvalue);
                if(git == groups.end())
                {
                    git = groups.insert(pair<_db_id, _grec_report_full>(gvalue, _grec_report_full())).first;

                    if      (out_mode==by_opers) git->second.name = c.oper_name;
                    else if (out_mode==by_terms)
                    {
                        git->second.name = c.term_vname;
                        git->second.info = c.term_place;
                    }
                    else if (out_mode==by_queues) git->second.name = c.q_vname;

                    git->second.tcnt = 0;
                    git->second.cnt_serv_ok=0;
                    git->second.cnt_serv_err=0;
                    git->second.cnt_no_serv_ok=0;
                    git->second.cnt_no_serv_err=0;
                    git->second.serv_for = 0;
                    git->second.wait_for = 0;
                }

                // & update it
                git->second.serv_for += c.serv_for;
                git->second.wait_for += c.wait_for;
                git->second.tcnt++;

                if (c.end_code==0)      git->second.cnt_serv_ok++;
                else if (c.end_code==1) git->second.cnt_no_serv_ok++;
                else if (c.end_code==2)
                {
                    if (c.serv_for) git->second.cnt_serv_err++; else git->second.cnt_no_serv_err++;
                }

                // update global stat
                gall.serv_for += c.serv_for;
                gall.wait_for += c.wait_for;
                gall.tcnt++;

                if (c.end_code==0)      gall.cnt_serv_ok++;
                else if (c.end_code==1) gall.cnt_no_serv_ok++;
                else if (c.end_code==2)
                {
                    if (c.serv_for) gall.cnt_serv_err++; else gall.cnt_no_serv_err++;
                }
            }
            // end of fetching

            // begin out data to html
            map<_db_id, _grec_report_full>::iterator git = groups.begin();
            int h, m, s;

            // out qs or opers begin section
            if      (out_mode==by_opers) hr->outSection("GOPER_BEGIN", tmp);
            else if (out_mode==by_terms) hr->outSection("GTERM_BEGIN", tmp);
            else if (out_mode==by_queues) hr->outSection("GQ_BEGIN", tmp);
            res+=tmp;

            while(git != groups.end())
            {
                hr->setVar("NAME", git->second.name);
                hr->setVar("INFO", git->second.info);

                if(git->second.tcnt)
                {
                    hr->setVar("COUNT", git->second.tcnt);
                    hr->setVar("COUNT_SERV_OK", git->second.cnt_serv_ok);
                    hr->setVar("COUNT_SERV_ERR", git->second.cnt_serv_err);
                    hr->setVar("COUNT_NO_SERV_OK", git->second.cnt_no_serv_ok);
                    hr->setVar("COUNT_NO_SERV_ERR", git->second.cnt_no_serv_err);
                    hr->setVar("COUNT_ERR", git->second.cnt_no_serv_err+git->second.cnt_serv_err);

                    make_hms_from_ts(git->second.wait_for, h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("SUM_WAIT_TIME", tmp_s);

                    make_hms_from_ts(git->second.wait_for / git->second.tcnt, h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("AVG_WAIT_TIME", tmp_s);

                    make_hms_from_ts(git->second.serv_for, h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("SUM_SERV_TIME", tmp_s);

                    if (git->second.cnt_serv_ok + git->second.cnt_serv_err)
                    {
                        make_hms_from_ts(git->second.serv_for / (git->second.cnt_serv_ok + git->second.cnt_serv_err), h, m, s);
                        sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                        hr->setVar("AVG_SERV_TIME", tmp_s);
                    }
                    else
                    {
                        hr->setVar("AVG_SERV_TIME","-");
                    }
                }
                else
                {
                    hr->setVar("COUNT", 0);
                    hr->setVar("SUM_WAIT_TIME", "-");
                    hr->setVar("SUM_SERV_TIME", "-");
                    hr->setVar("AVG_WAIT_TIME", "-");
                    hr->setVar("AVG_SERV_TIME", "-");
                }

                if      (out_mode==by_opers) hr->outSection("GOPER_LINE", tmp);
                else if (out_mode==by_terms) hr->outSection("GTERM_LINE", tmp);
                else if (out_mode==by_queues) hr->outSection("GQ_LINE", tmp);
                res+=tmp;

                git++;
            }

            if(gall.tcnt)
            {
                hr->setVar("COUNT", gall.tcnt);
                hr->setVar("COUNT_SERV_OK", gall.cnt_serv_ok);
                hr->setVar("COUNT_SERV_ERR", gall.cnt_serv_err);
                hr->setVar("COUNT_NO_SERV_OK", gall.cnt_no_serv_ok);
                hr->setVar("COUNT_NO_SERV_ERR", gall.cnt_no_serv_err);
                hr->setVar("COUNT_ERR", gall.cnt_no_serv_err+gall.cnt_serv_err);

                make_hms_from_ts(gall.wait_for, h, m, s);
                sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                hr->setVar("SUM_WAIT_TIME", tmp_s);

                make_hms_from_ts(gall.wait_for / gall.tcnt, h, m, s);
                sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                hr->setVar("AVG_WAIT_TIME", tmp_s);

                make_hms_from_ts(gall.serv_for, h, m, s);
                sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                hr->setVar("SUM_SERV_TIME", tmp_s);

                if (gall.cnt_serv_ok + gall.cnt_serv_err)
                {
                    make_hms_from_ts(gall.serv_for / (gall.cnt_serv_ok + gall.cnt_serv_err), h, m, s);
                    sprintf(tmp_s, "%02i:%02i:%02i", h, m, s);
                    hr->setVar("AVG_SERV_TIME", tmp_s);
                }
                else
                {
                    hr->setVar("AVG_SERV_TIME","-");
                }
            }
            else
            {
                hr->setVar("COUNT", 0);
                hr->setVar("COUNT_SERV_OK", 0);
                hr->setVar("COUNT_SERV_ERR", 0);
                hr->setVar("COUNT_NO_SERV_OK", 0);
                hr->setVar("COUNT_NO_SERV_ERR", 0);
                hr->setVar("COUNT_ERR", 0);
                hr->setVar("SUM_WAIT_TIME", "-");
                hr->setVar("SUM_SERV_TIME", "-");
                hr->setVar("AVG_WAIT_TIME", "-");
                hr->setVar("AVG_SERV_TIME", "-");
            }

            if      (out_mode==by_opers) hr->outSection("GOPER_END", tmp);
            else if (out_mode==by_terms) hr->outSection("GTERM_END", tmp);
            else if (out_mode==by_queues) hr->outSection("GQ_END", tmp);

            res+=tmp;
        }

        delete st;
        st = NULL;

        tr->commit();
        delete tr;
        tr = NULL;

        delete db;
        db = NULL;

        hr->outSection("END", tmp); // report footer
        res += tmp;

        if(hh->req_get_param(params, values, "export") == "1")
        {
            char fname[256];
            sprintf(fname, "%04i_%02i_%02i__%04i_%02i_%02i.xls",
                    begin_t.year(), begin_t.month(), begin_t.day(),
                    end_t.year(), end_t.month(), end_t.day());

            ofstream f_tmp;
            f_tmp.open((_session->root_dir() + "tmp/report_sum_" + fname).c_str(), ios::out | ios::binary);
            f_tmp.write(res.data(), res.length());
            f_tmp.close();

            hh->http_file(_session->root_dir(), "/comctl/redir_to_file.html", res);
            hr->setTemplateText(res.c_str());
            hr->setVar("FILENAME", string("/tmp/report_sum_") + fname);
            hr->outSection("MAIN", res);
        }
    }
    catch(_db_error& e)
    {
        if(st) { try { delete st; } catch(...) {} }
        if(tr) { try { tr->rollback(); delete tr; } catch(...) {} }
        if(db) { try { delete db; } catch(...) {} }
        _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
    }

    e_code = 0;

    hh->release();
    hr->release();

    return e_code;
}

/*
int _http_report::report_by_hours(list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    char tmp_s[1024];

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    _db* db = NULL;
    _transaction* tr = NULL;
    _statement* st = NULL;

    try
    {
        db = create_db_oracle();

        tr = db->transaction();
        tr->start();

        // LOAD TEMPLATE
        if(hh->req_get_param(params, values, "export") == "1")
        {
            hr->setTemplate((_session->root_dir() + "reports/report_by_hours_export.html").c_str());
        }
        else
        {
            hh->http_file(_session->root_dir(), "/reports/report_by_hours.html", res);
            hr->setTemplateText(res.c_str());
        }

        std::string tmp;

        // PARSE FILTERS

        _time begin_t(atoi(hh->req_get_param(params, values, "begin_y").c_str()),
                      atoi(hh->req_get_param(params, values, "begin_m").c_str()),
                      atoi(hh->req_get_param(params, values, "begin_d").c_str()),
                      0, 0, 0, 0);
        if(hh->req_get_param(params, values, "begin_d") != "")
        {
            hr->setVar("BEGIN_D", begin_t.day());
            hr->setVar("BEGIN_M", begin_t.month());
            hr->setVar("BEGIN_Y", begin_t.year());

        }

        _time end_t = begin_t;
        end_t.tick(24 * 60 * 60 * 1000 - 1);

        if(hh->req_get_param(params, values, "end_d") != "")
        {
            end_t =  _time(atoi(hh->req_get_param(params, values, "end_y").c_str()),
                           atoi(hh->req_get_param(params, values, "end_m").c_str()),
                           atoi(hh->req_get_param(params, values, "end_d").c_str()),
                           23, 59, 59, 999);
        }

        if(hh->req_get_param(params, values, "end_d") != "" || hh->req_get_param(params, values, "begin_d") != "")
        {
            hr->setVar("END_D", end_t.day());
            hr->setVar("END_M", end_t.month());
            hr->setVar("END_Y", end_t.year());
        }

        if(hh->req_get_param(params, values, "begin_h") != "")
        {
            begin_t.hour(atoi(hh->req_get_param(params, values, "begin_h").c_str()));
            hr->setVar("BEGIN_H", begin_t.hour());

            if (hh->req_get_param(params,values,"begin_n")!="")
            {
                begin_t.minute(atoi(hh->req_get_param(params, values, "begin_n").c_str()));
                hr->setVar("BEGIN_N", begin_t.minute());
            }
        }

        if(hh->req_get_param(params, values, "end_h") != "")
        {
            end_t.hour(atoi(hh->req_get_param(params, values, "end_h").c_str()));
            hr->setVar("END_H", end_t.hour());

            if(hh->req_get_param(params, values, "end_n") != "")
            {
                end_t.minute(atoi(hh->req_get_param(params, values, "end_n").c_str()));
                hr->setVar("END_N", end_t.minute());
            }
            else
            {
                end_t.minute(0);
                hr->setVar("END_N", 0);
            }
        }

        st = tr->statement();

        string sel_arr;

        // OPERATOR FILTER

        _mfilter oper_filter;
        st->prepare("select ID,FIO_F,FIO_I,FIO_O from GL_USERS");
        st->execute();
        string uname_i, uname_o;
        while(st->fetch())
        {
            _mfilter_rec r;

            st->pop(r.id);
            st->pop(r.name);
            st->pop(uname_i);
            st->pop(uname_o);
            if(uname_i.length())
            {
                r.name += " ";
                r.name += uname_i[0];
                r.name += ".";
            }
            if(uname_o.length())
            {
                r.name += " ";
                r.name += uname_o[0];
                r.name += ".";
            }
            r.is_selected = false;

            oper_filter.push_back(r);
        }
        string oper_sel_ids_str = hh->req_get_param(params, values, "oper_sel_ids");
        if(oper_sel_ids_str == "0")
            oper_sel_ids_str = "";
        set_select_mfilter(oper_filter, oper_sel_ids_str);
        make_array_mfilter("oper_sel_arr", oper_filter, sel_arr);
        hr->setVar("OPER_SEL_ARRAY", sel_arr);

        // terminal filter

        _mfilter term_filter;
        st->prepare("select ID,VNAME,PLACE from TERMS");//DNS_NAME?
        st->execute();
        string term_place;
        while(st->fetch())
        {
            _mfilter_rec r;
            st->pop(r.id);
            st->pop(r.name);
            st->pop(term_place);
            if(term_place.length())
            {
                r.name += " (";
                r.name += term_place[0];
                r.name += ")";
            }
            r.is_selected = false;

            term_filter.push_back(r);
        }
        string term_sel_ids_str = hh->req_get_param(params, values, "term_sel_ids");
        if(term_sel_ids_str == "0")
            term_sel_ids_str = "";
        set_select_mfilter(term_filter, term_sel_ids_str);
        make_array_mfilter("term_sel_arr", term_filter, sel_arr);
        hr->setVar("TERM_SEL_ARRAY", sel_arr);

        // QUEUE FILTER

        _mfilter queue_filter;
        st->prepare("select ID,VNAME from QSERVER_QS");
        st->execute();
        while(st->fetch())
        {
            _mfilter_rec r;
            st->pop(r.id);
            st->pop(r.name);
            r.is_selected=false;

            queue_filter.push_back(r);
        }
        string queue_sel_ids_str=hh->req_get_param(params, values, "q_sel_ids");
        if (queue_sel_ids_str == "0") queue_sel_ids_str = "";
        set_select_mfilter(queue_filter, queue_sel_ids_str);
        make_array_mfilter("queue_sel_arr", queue_filter, sel_arr);
        hr->setVar("queue_sel_arrAY", sel_arr);

        hr->outSection("MAIN", res);

        if(hh->req_get_param(params, values, "begin_d") != "")
        {
            _selector sel;
            sel.add_var("C.BEGIN_AT");
            sel.add_var("C.ANSWER_AT");
            sel.add_var("C.END_AT");
            sel.add_var("C.END_CODE");
            sel.add_var("C.ID");
//TODO            sel.add_var("Q.ID");
//            sel.add_var("Q.VNAME");
            sel.add_var("U.ID");
            sel.add_var("U.FIO_F");
            sel.add_var("U.FIO_I");
            sel.add_var("U.FIO_O");
            sel.add_var("T.ID");
            sel.add_var("T.VNAME");
            sel.add_var("T.PLACE");

            sel.add_from("CALLS C");
//TODO            sel.add_from("QSERVER_QS");
            sel.add_from("GL_USERS U");
            sel.add_from("TERMS T");

//TODO            sel.add_cond("C.Q_ID=Q.ID");
            sel.add_cond("C.OPER_ID=U.ID");
            sel.add_cond("C.TERM_ID=T.ID");
            sel.add_cond("C.BEGIN_AT > :bs<bigint>");
            sel.add_cond("C.BEGIN_AT < :es<bigint>");

            if(oper_sel_ids_str != "")
                sel.add_cond(string("U.ID in (") + oper_sel_ids_str + ")");
            if(term_sel_ids_str != "")
                sel.add_cond(string("T.ID in (") + term_sel_ids_str + ")");
//TODO			if(queue_sel_ids_str != "")
//                sel.add_cond(string("Q.ID in (") + queue_sel_ids_str + ")");

            sel.set_order("C.BEGIN_AT");

//            MLOG(0) << sel.sql_ref() << endl;

            st->prepare(sel.sql_ref());
            st->push(begin_t);
            st->push(end_t);
            st->execute();

            _drec_report_by_hours dall;
            map<_time, _drec_report_by_hours> days;

            while(st->fetch())
            {
                _call_rec_report_full c;

                st->pop(c.begin_at);
                st->pop(c.answer_at);
                st->pop(c.end_at);
                st->pop(c.call_id);
//TODO                st->pop(c.q_id);
//                st->pop(c.q_vname);
                st->pop(c.oper_id);
                st->pop(c.oper_name);
                st->pop(uname_i);
                st->pop(uname_o);
                if(uname_i.length())
                {
                    c.oper_name += " ";
                    c.oper_name += uname_i[0];
                    c.oper_name += ".";
                }
                if(uname_o.length())
                {
                    c.oper_name += " ";
                    c.oper_name += uname_o[0];
                    c.oper_name += ".";
                }
                st->pop(c.term_id);
                st->pop(c.term_vname);
                st->pop(c.term_place);

                c.wait_for = c.answer_at.stamp() - c.begin_at.stamp();
                c.wait_for /= 10000;

                c.serv_for = c.end_at.stamp() - c.answer_at.stamp();
                c.serv_for /= 10000;

                _time dtime;
                dtime.year(c.answer_at.year());
                dtime.month(c.answer_at.month());
                dtime.day(t.answer_at.day());

                map<_time, _drec_report_by_hours>::iterator dit = days.find(dtime);
                if(dit == days.end())
                {
                    dit = days.insert(pair<_time, _drec_report_by_hours>(dtime, _drec_report_by_hours())).first;
                    dit->second.all_cnt = 0;
                }

                dit->second.all_cnt++;

                _time htime = dtime;
                htime.hour(c.answer_at.hour());

                map<int, int>::iterator hit = dit->second.hours.find(htime.hour());
                if(hit == dit->second.hours.end())
                    hit = dit->second.hours.insert(pair<int, int>(htime.hour(), 0)).first;
                hit->second++;

                dall.all_cnt++;
                hit = dall.hours.find(htime.hour());
                if(hit == dall.hours.end())
                    hit = dall.hours.insert(pair<int, int>(htime.hour(), 0)).first;
                hit->second++;
            }

            hr->outSection("HEAD_BEGIN", tmp);
            res += tmp;

            map<int, int>::iterator hit = dall.hours.begin();
            int bhour = hit->first;
            int ehour = bhour;
            while(hit != dall.hours.end())
            {
                ehour = hit->first;
                hit++;
            }

            int hour;
            for(hour = bhour; hour<ehour + 1; hour++)
            {
                hr->setVar("IB", hour);
                hr->setVar("IE", hour + 1);

                hr->outSection("HEAD_LINE", tmp);
                res += tmp;
            }

            hr->outSection("HEAD_END", tmp);
            res += tmp;

            map<_time, _drec_report_by_hours>::iterator dit = days.begin();
            while(dit != days.end())
            {
                hr->setVar("DAY", dit->first.day());
                hr->setVar("MONTH", dit->first.month());
                hr->setVar("YEAR", dit->first.year());

                hr->outSection("D_BEGIN", tmp);
                res += tmp;

                int hour;
                for(hour = bhour; hour<ehour + 1; hour++)
                {
                    map<int, int>::iterator hit = dit->second.hours.find(hour);
                    if(hit != dit->second.hours.end())
                        hr->setVar("COUNT", hit->second);
                    else
                        hr->setVar("COUNT", 0);
                    hr->outSection("D_LINE", tmp);
                    res += tmp;
                }

                hr->setVar("COUNT", dit->second.all_cnt);
                hr->outSection("D_END", tmp);
                res += tmp;

                dit++;
            }

            hr->outSection("E_BEGIN", tmp);
            res += tmp;

            for(hour = bhour; hour<ehour + 1; hour++)
            {
                map<int, int>::iterator hit = dall.hours.find(hour);
                if(hit != dall.hours.end())
                    hr->setVar("COUNT", hit->second);
                else
                    hr->setVar("COUNT", 0);
                hr->outSection("E_LINE", tmp);
                res += tmp;
            }

            hr->setVar("COUNT", dall.all_cnt);
            hr->outSection("E_END", tmp);
            res += tmp;
        }

        delete st;
        st = NULL;

        tr->commit();
        delete tr;
        tr = NULL;

        delete db;
        db = NULL;

        hr->outSection("END", tmp);
        res += tmp;

        if(hh->req_get_param(params, values, "export") == "1")
        {
            char fname[256];
            sprintf(fname, "%04i_%02i_%02i__%04i_%02i_%02i.xls",
                           begin_t.year(), begin_t.month(), begin_t.day(),
                           end_t.year(), end_t.month(), end_t.day());

            ofstream f_tmp;
            f_tmp.open((_session->root_dir() + "tmp/report_by_hours_" + fname).c_str(), ios::out | ios::binary);
            f_tmp.write(res.data(), res.length());
            f_tmp.close();

            hh->http_file(_session->root_dir(), "/comctl/redir_to_file.html", res);
            hr->setTemplateText(res.c_str());
            hr->setVar("FILENAME", string("/tmp/report_by_hours_") + fname);
            hr->outSection("MAIN", res);
        }
    }
    catch(_db_error& e)
    {
        if(st) { try { delete st; } catch(...) {} }
        if(tr) { try { tr->rollback(); delete tr; } catch(...) {} }
        if(db) { try { delete db; } catch(...) {} }
        _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
    }

    e_code = 0;

    hh->release();
    hr->release();

    return e_code;
}
*/
