#ifndef __HTTP_REPORT_OBJ__
#define __HTTP_REPORT_OBJ__

#include <string>
//#include "../common/db_obj/report.hpp"
#include "../session.hpp"

// --------------------------------------------------

class _http_report : public _client_object
{
    private:

        int report_full(list<std::string>& params,list<std::string>& values,std::string& res);
        int report_sum(list<std::string>& params,list<std::string>& values,std::string& res);
        int report_call_info(list<std::string>& params,list<std::string>& values,std::string& res);
        int report_by_hours(list<std::string>& params,list<std::string>& values,std::string& res);

    protected:
    public:

      _http_report(_client_session* session);
      virtual ~_http_report();

      virtual int cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res);
};

#endif
