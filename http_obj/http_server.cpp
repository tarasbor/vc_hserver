#include <net_http.h>
#include <htmlrep.hpp>
#ifdef VC_ORACLE
    #include <obj_db5/base_oracle.hpp>
#endif
#ifdef VC_DB2
    #include <obj_db5/base_db2.hpp>
#endif

#include "http_server.hpp"

// --------------------------------------

int _http_mserver::cmd(std::string& act, list<std::string>& params, list<std::string> &values, std::string& res)
{
    int e_code=-1;

    _http_help *hh = create_http_help();
    _report *hr = create_html_report();
    std::string tmp;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if       (act=="mserver.new")
    {
        hh->http_file(_session->root_dir(),"/server/mserver.html",res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url","/refresh?act=refresh");
        hr->setVar("MSERVER_CONTROL_IP","any");
        hr->setVar("MSERVER_CONTROL_PORT","4600");
        hr->setVar("MSERVER_LOG_LEVELS","common=5,db=5,net=5");
        hr->outSection("MAIN",res);

        e_code=0;
    }
    else if  (act=="mserver.show")
    {
        try
        {
            _transaction *tr = db->transaction();
            tr->start();

            _db_cl::_mserver *ms = new _db_cl::_mserver(hh->req_get_param(params, values, "mserver_id"), tr);

            hh->http_file(_session->root_dir(),"/server/mserver.html",res);
            hr->setTemplateText(res.c_str());

            hr->setVar("MSERVER_ID", ms->id().value());
            hr->setVar("MSERVER_DNS_NAME", ms->dns_name());
            hr->setVar("MSERVER_CONTROL_IP", ms->control_ip());
            hr->setVar("MSERVER_CONTROL_PORT", ms->control_port());
            hr->setVar("MSERVER_LOG_LEVELS", ms->log_levels());
            if(ms->is_enabled())
                hr->setVar("MSERVER_IS_ENABLED", "checked");

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            hr->outSection("MAIN", res);

            delete ms;

            tr->commit();
            delete tr;
        }
        catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }

        e_code=0;
    }
    else if  (act=="mserver.apply")
    {
        try
        {
            _transaction *tr = db->transaction(); //(IBPP::amWrite, IBPP::ilReadCommitted, IBPP::lrWait, IBPP::TFF(0));
            tr->start();

            _db_id ms_id(hh->req_get_param(params,values,"mserver_id"));
            _db_cl::_mserver *ms;
            if (!ms_id.is_set()) ms=new _db_cl::_mserver(tr); else ms=new _db_cl::_mserver(ms_id,tr);

            ms->dns_name(hh->req_get_param(params,values,"mserver_dns_name"));
            ms->control_ip(hh->req_get_param(params,values,"mserver_control_ip"));
            ms->control_port(atoi(hh->req_get_param(params,values,"mserver_control_port").c_str()));
            ms->log_levels(hh->req_get_param(params,values,"mserver_log_levels").c_str());
            ms->is_enabled(hh->req_get_param(params,values,"mserver_is_enabled") == "1" ? 1 : 0);

            delete ms;

            tr->commit();
            delete tr;

            hh->http_file(_session->root_dir(),"/comctl/done.html",res);
        }
        catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }

        e_code=0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

int _http_hserver::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res)
{
    int e_code=-1;

    _http_help *hh=create_http_help();
    _report *hr=create_html_report();
    std::string tmp;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if       (act=="hserver.new")
    {
        hh->http_file(_session->root_dir(),"/server/hserver.html",res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url","/refresh?act=refresh");
        hr->setVar("HSERVER_CONTROL_IP","any");
        hr->setVar("HSERVER_CONTROL_PORT","80");
        hr->setVar("HSERVER_LOG_LEVELS","common=5,db=5,net=5");
        hr->outSection("MAIN",res);

        e_code=0;
    }
    else if  (act=="hserver.show")
    {
        try
        {
            _transaction *tr = db->transaction();
            tr->start();

            _db_cl::_hserver *hs=new _db_cl::_hserver(hh->req_get_param(params,values,"hserver_id"),tr);

            hh->http_file(_session->root_dir(),"/server/hserver.html",res);
            hr->setTemplateText(res.c_str());

            hr->setVar("HSERVER_ID",hs->id().value());
            hr->setVar("HSERVER_DNS_NAME",hs->dns_name());
            hr->setVar("HSERVER_CONTROL_IP",hs->control_ip());
            hr->setVar("HSERVER_CONTROL_PORT",hs->control_port());
            hr->setVar("HSERVER_DOC_ROOT",hs->doc_root());
            hr->setVar("HSERVER_LOG_LEVELS",hs->log_levels());

            hr->setVarNoRep("refresh_url","/refresh?act=refresh");

            hr->outSection("MAIN",res);

            delete hs;

            tr->commit();
            delete tr;
        }
        catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }

        e_code=0;
    }
    else if  (act=="hserver.apply")
    {
        try
        {
            _transaction *tr = db->transaction(); //(IBPP::amWrite, IBPP::ilReadCommitted, IBPP::lrWait, IBPP::TFF(0));
            tr->start();

            _db_id hs_id(hh->req_get_param(params,values,"hserver_id"));
            _db_cl::_hserver *hs = hs_id.is_set() ? new _db_cl::_hserver(hs_id,tr) : new _db_cl::_hserver(tr);
//            if(!hs_id.is_set())
//                hs = new _db_cl::_hserver(tr);
//            else
//                hs = new _db_cl::_hserver(hs_id,tr);

            hs->dns_name(hh->req_get_param(params,values,"hserver_dns_name"));
            hs->control_ip(hh->req_get_param(params,values,"hserver_control_ip"));
            hs->control_port(atoi(hh->req_get_param(params,values,"hserver_control_port").c_str()));
            hs->doc_root(hh->req_get_param(params,values,"hserver_doc_root"));
            hs->log_levels(hh->req_get_param(params,values,"hserver_log_levels").c_str());

            delete hs;

            tr->commit();
            delete tr;

            hh->http_file(_session->root_dir(),"/comctl/done.html",res);
        }
        catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }

        e_code=0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

int _http_qserver::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res)
{
    int e_code=-1;

    _http_help *hh=create_http_help();
    _report *hr=create_html_report();
    std::string tmp;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if      (act=="qserver.new")
    {
        hh->http_file(_session->root_dir(),"/server/qserver.html",res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url","/refresh?act=refresh");
        hr->setVar("QSERVER_CONTROL_IP","any");
        hr->setVar("QSERVER_CONTROL_PORT","4601");
        hr->setVar("QSERVER_DATA_IP","any");
        hr->setVar("QSERVER_DATA_PORT","4611");
        hr->setVar("QSERVER_LOG_LEVELS","common=5,db=5,net=5");
        hr->outSection("MAIN",res);

        e_code=0;
    }
    else if (act=="qserver.show")
    {
        try
        {
            _transaction *tr = db->transaction();
            tr->start();

            _db_cl::_qserver *qs=new _db_cl::_qserver(hh->req_get_param(params,values,"qserver_id"),tr);

            hh->http_file(_session->root_dir(),"/server/qserver.html",res);
            hr->setTemplateText(res.c_str());

            hr->setVar("QSERVER_ID",qs->id().value());
            hr->setVar("QSERVER_DNS_NAME",qs->dns_name());
            hr->setVar("QSERVER_CONTROL_IP",qs->control_ip());
            hr->setVar("QSERVER_CONTROL_PORT",qs->control_port());
            hr->setVar("QSERVER_DATA_IP",qs->data_ip());
            hr->setVar("QSERVER_DATA_PORT",qs->data_port());
            hr->setVar("QSERVER_LOG_LEVELS",qs->log_levels());

            hr->setVarNoRep("refresh_url","/refresh?act=refresh");

            hr->outSection("MAIN",res);

            delete qs;

            tr->commit();
            delete tr;
        }
        catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }

        e_code=0;
    }
    else if (act=="qserver.apply")
    {
        try
        {
            _transaction *tr = db->transaction(); //(IBPP::amWrite, IBPP::ilReadCommitted, IBPP::lrWait, IBPP::TFF(0));
            tr->start();

            _db_id qs_id(hh->req_get_param(params,values,"qserver_id"));
            _db_cl::_qserver *qs;
            if (!qs_id.is_set()) qs=new _db_cl::_qserver(tr); else qs=new _db_cl::_qserver(qs_id,tr);

            qs->dns_name(hh->req_get_param(params,values,"qserver_dns_name"));
            qs->control_ip(hh->req_get_param(params,values,"qserver_control_ip"));
            qs->control_port(atoi(hh->req_get_param(params,values,"qserver_control_port").c_str()));
            qs->data_ip(hh->req_get_param(params,values,"qserver_data_ip"));
            qs->data_port(atoi(hh->req_get_param(params,values,"qserver_data_port").c_str()));
            qs->log_levels(hh->req_get_param(params,values,"qserver_log_levels").c_str());

            delete qs;

            tr->commit();
            delete tr;

            hh->http_file(_session->root_dir(),"/comctl/done.html",res);
        }
        catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }

        e_code=0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

int _http_queue::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res)
{
    int e_code=-1;

    _http_help *hh=create_http_help();
    _report *hr=create_html_report();
    std::string tmp;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    char tmp_s[128];
    int i;

    if       (act == "queue.new")
    {
        hh->http_file(_session->root_dir(),"/server/queue.html",res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url","/refresh?act=refresh");
        hr->setVar("QSERVER_ID",hh->req_get_param(params,values,"qserver_id"));
        hr->setVar("QSERVER_DNS_NAME",hh->req_get_param(params,values,"qserver_dns_name"));

        // default values
		for(i=1;i<8;i++)
		{
			sprintf(tmp_s,"BH_%i",i);
			hr->setVar(tmp_s,0);

			sprintf(tmp_s,"BM_%i",i);
			hr->setVar(tmp_s,0);

			sprintf(tmp_s,"EH_%i",i);
			hr->setVar(tmp_s,23);

			sprintf(tmp_s,"EM_%i",i);
			hr->setVar(tmp_s,59);
		}

        hr->outSection("MAIN",res);

        e_code=0;
    }
    else if  (act == "queue.show")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();

            _db_id q_id(hh->req_get_param(params, values, "queue_id"));
            _db_cl::_q* q = new _db_cl::_q(q_id, tr);

            _db_cl::_qserver* qsrv = q->qserver();

            hh->http_file(_session->root_dir(), "/server/queue.html", res);
            hr->setTemplateText(res.c_str());

            hr->setVar("QSERVER_ID", qsrv->id().value());
            hr->setVar("QSERVER_DNS_NAME", qsrv->dns_name());

            hr->setVar("QUEUE_ID", q->id().value());
            hr->setVar("QUEUE_NAME", q->name());
            hr->setVar("QUEUE_VNAME", q->vname());

            // default values
			for(i=1;i<8;i++)
			{
				sprintf(tmp_s,"BH_%i",i);
				hr->setVar(tmp_s,0);

				sprintf(tmp_s,"BM_%i",i);
				hr->setVar(tmp_s,0);

				sprintf(tmp_s,"EH_%i",i);
				hr->setVar(tmp_s,23);

				sprintf(tmp_s,"EM_%i",i);
				hr->setVar(tmp_s,59);
			}

			_db_cl::_work_times_list::iterator *wt_it=q->work_times();
			while(!wt_it->eof())
			{
                sprintf(tmp_s,"BH_%i",(*wt_it)->on_day());
				hr->setVar(tmp_s,(*wt_it)->start_at().hour());

				sprintf(tmp_s,"BM_%i",(*wt_it)->on_day());
				hr->setVar(tmp_s,(*wt_it)->start_at().minute());

				sprintf(tmp_s,"EH_%i",(*wt_it)->on_day());
				hr->setVar(tmp_s,(*wt_it)->end_at().hour());

				sprintf(tmp_s,"EM_%i",(*wt_it)->on_day());
				hr->setVar(tmp_s,(*wt_it)->end_at().minute());

                (*wt_it)++;
			}
			delete wt_it;

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            hr->outSection("MAIN", res);

            delete qsrv;
            delete q;

            tr->commit();
            delete tr;
        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if  (act == "queue.apply")
    {
        try
        {
            _transaction *tr = db->transaction(); //(IBPP::amWrite, IBPP::ilReadCommitted, IBPP::lrWait, IBPP::TFF(0));
            tr->start();

            _db_id q_id(hh->req_get_param(params, values, "queue_id"));
            _db_cl::_q *q;
            q = q_id.is_set() ? new _db_cl::_q(q_id, tr) : new _db_cl::_q(tr);

            q->name(hh->req_get_param(params, values, "queue_name"));
            q->vname(hh->req_get_param(params, values, "queue_vname"));

            _db_cl::_qserver *qsrv = new _db_cl::_qserver(hh->req_get_param(params, values, "qserver_id"), tr);
            q->qserver(qsrv);

            for(i=1;i<8;i++)
			{
				int val;

				_time start_at;
				start_at.set_now();

				_time end_at;
				end_at.set_now();

				sprintf(tmp_s,"bh_%i",i);
				val=atoi(hh->req_get_param(params,values,tmp_s).c_str());
				start_at.hour(val);

				sprintf(tmp_s,"bm_%i",i);
				val=atoi(hh->req_get_param(params,values,tmp_s).c_str());
				start_at.minute(val);

				sprintf(tmp_s,"eh_%i",i);
				val=atoi(hh->req_get_param(params,values,tmp_s).c_str());
				end_at.hour(val);

				sprintf(tmp_s,"em_%i",i);
				val=atoi(hh->req_get_param(params,values,tmp_s).c_str());
				end_at.minute(val);

				bool find=false;
				_db_cl::_work_times_list::iterator *wt_it=q->work_times();
				while(!wt_it->eof())
				{
                    if ((*wt_it)->on_day()==i)
                    {
                        find=true;
                        break;
                    }
                    (*wt_it)++;
				}

				_db_cl::_work_time *wt;
				if (!find) wt=new _db_cl::_work_time(tr); else wt=(*wt_it);

				wt->start_at(start_at);
				wt->end_at(end_at);
				wt->on_day(i);

				q->work_time(*wt);
                if (!find) delete wt;

                delete wt_it;
			}

            delete qsrv;
            delete q;

            tr->commit();
            delete tr;

            hh->http_file(_session->root_dir(),"/comctl/done.html",res);
        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

int _http_queues_list::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res)
{
    int e_code=-1;
    std::string tmp;

    _http_help *hh=create_http_help();
    _report *hr=create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if  (act=="queues_list.show")
    {
        _db_id qs_id(hh->req_get_param(params,values,"qserver_id"));
        if (!qs_id.is_set())
        {
            _http_db_error::out(_session->root_dir(),"/err/common.html","Q Server ID was not setted",res);
        }
        else
        {
            try
            {
                _transaction *tr = db->transaction();
                tr->start();

                hh->http_file(_session->root_dir(),"/server/queues_list.html",res);
                hr->setTemplateText(res.c_str());

                _db_cl::_qserver *qsrv = new _db_cl::_qserver(qs_id, tr);

                hr->setVar("QSERVER_ID", qsrv->id().value());
                hr->setVar("QSERVER_DNS_NAME", qsrv->dns_name());
                hr->setVar("SELECT", hh->req_get_param(params, values, "select"));
                hr->outSection("MAIN", res);

//                _db_cl::_qs_list *q_list = qsrv->queues();
//                _db_cl::_qs_list::iterator *qit = q_list->begin();
                _db_cl::_qs_list::iterator *qit = qsrv->queues();
                int cnt = 0;
                while(!qit->eof())
                {
                    hr->setVar("LINE_MOD",cnt%2);
                    hr->setVar("Q_ID",(*qit)->id().value());
                    hr->setVar("Q_NAME",(*qit)->name());
                    hr->setVar("Q_VNAME",(*qit)->vname());
                    hr->outSection("LINE",tmp);
                    res+=tmp;

                    (*qit)++;
                    cnt++;
                }
//                delete q_list;

                hr->outSection("END",tmp);
                res += tmp;

                delete qsrv;

                tr->commit();
                delete tr;
            }
            catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

int _http_storage::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res)
{
    int e_code=-1;

    _http_help *hh = create_http_help();
    _report *hr = create_html_report();
    std::string tmp;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if       (act == "storage.new")
    {
        hh->http_file(_session->root_dir(),"/server/storage.html",res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
        hr->setVar("QSERVER_ID", hh->req_get_param(params, values, "qserver_id"));
        hr->outSection("MAIN", res);

        e_code=0;
    }
    else if  (act == "storage.show")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();

            _db_id q_id(hh->req_get_param(params, values, "storage_id"));
            _db_cl::_storage* strge = new _db_cl::_storage(q_id, tr);

            _db_cl::_qserver* qsrv = strge->qserver();

            hh->http_file(_session->root_dir(), "/server/storage.html", res);
            hr->setTemplateText(res.c_str());

            hr->setVar("QSERVER_ID", qsrv->id().value());
            hr->setVar("QSERVER_DNS_NAME", qsrv->dns_name());

            hr->setVar("STORAGE_ID", strge->id().value());
            hr->setVar("STORAGE_PATH", strge->path());
            hr->setVar("STORAGE_MTYPES", strge->mtypes());
            hr->setVar("STORAGE_MAX_SIZE", strge->max_size());

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            hr->outSection("MAIN", res);

            delete qsrv;
            delete strge;

            tr->commit();
            delete tr;
        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if  (act == "storage.apply")
    {
        try
        {
            _transaction *tr = db->transaction();
            tr->start();

            _db_id storage_id(hh->req_get_param(params, values, "storage_id"));
            _db_cl::_storage* storage;
            storage = storage_id.is_set() ? new _db_cl::_storage(storage_id, tr) : new _db_cl::_storage(tr);

            string path = hh->req_get_param(params, values, "storage_path");
            if(*path.rbegin() == '\\') //remove last \ symbol to prevent path's link broken in list of storages
                path.erase(path.length() - 1, 1);
            storage->path(path);
            storage->mtypes(hh->req_get_param(params, values, "storage_mtypes"));
            storage->max_size(atoi(hh->req_get_param(params, values, "storage_max_size").c_str()));

            _db_cl::_qserver *qsrv = new _db_cl::_qserver(hh->req_get_param(params, values, "qserver_id"),tr);
            //_db_id qsrv_id(hh->req_get_param(params, values, "qserver_id"));

            storage->qserver(qsrv);

            delete qsrv;
            delete storage;

            tr->commit();
            delete tr;

            hh->http_file(_session->root_dir(), "/comctl/done.html", res);
        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

int _http_storages_list::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res)
{
    int e_code = -1;
    std::string tmp;

    _http_help *hh = create_http_help();
    _report *hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if  (act == "storages_list.show")
    {
        _db_id qsrv_id(hh->req_get_param(params, values, "qserver_id"));
        if(!qsrv_id.is_set())
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", "Q Server ID was not setted", res);
        }
        else
        {
            try
            {
                _transaction *tr = db->transaction();
                tr->start();

                hh->http_file(_session->root_dir(), "/server/storages_list.html", res);
                hr->setTemplateText(res.c_str());

                _db_cl::_qserver *qsrv = new _db_cl::_qserver(qsrv_id, tr);

                hr->setVar("QSERVER_ID", qsrv->id().value());
                hr->setVar("QSERVER_DNS_NAME", qsrv->dns_name());
                hr->setVar("SELECT", hh->req_get_param(params, values, "select"));
                hr->outSection("MAIN", res);

                _db_cl::_storages_list::iterator *sts_it = qsrv->storages();
                int cnt = 0;
                while(!sts_it->eof())
                {
                    hr->setVar("LINE_MOD",cnt%2);
                    hr->setVar("STORAGE_ID",(*sts_it)->id().value());
                    hr->setVar("STORAGE_PATH",(*sts_it)->path());
                    hr->setVar("STORAGE_MTYPES",(*sts_it)->mtypes());
                    hr->setVar("STORAGE_MAX_SIZE",(*sts_it)->max_size());
                    hr->outSection("LINE",tmp);
                    res += tmp;

                    (*sts_it)++;
                    cnt++;
                }

                hr->outSection("END", tmp);
                res += tmp;

                delete qsrv;

                tr->commit();
                delete tr;
            }
            catch(_db_error& e)
            {
                _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
            }
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

int _http_servers_list::cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res)
{
    int e_code = -1;
    std::string tmp;

    _http_help *hh=create_http_help();
    _report *hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "servers_list.show")
    {
        try
        {
            bool show_ms = hh->req_get_param(params,values,"show_ms") == "1";
            bool show_qs = hh->req_get_param(params,values,"show_qs") == "1";
            bool show_hs = hh->req_get_param(params,values,"show_hs") == "1";

            _transaction *tr = db->transaction();
            tr->start();

            hh->http_file(_session->root_dir(),"/server/servers_list.html", res);
            hr->setTemplateText(res.c_str());

            hr->outSection("MAIN", res);

            if(show_ms)
            {
                hr->outSection("MS_BEGIN", tmp);
                res += tmp;

                _db_cl::_mservers_list::iterator *ms_it = _db_cl::_mservers_list().begin(tr);
                int cnt = 0;
                while(!ms_it->eof())
                {
                    hr->setVar("LINE_MOD", cnt % 2);
                    hr->setVar("MS_ID", (*ms_it)->id().value());
                    hr->setVar("MS_DNS_NAME", (*ms_it)->dns_name());
                    hr->setVar("MS_IS_ENABLED", (*ms_it)->is_enabled() ? "+" : "");
                    hr->setVar("MS_CONTROL_IP",(*ms_it)->control_ip());
                    hr->setVar("MS_CONTROL_PORT",(*ms_it)->control_port());
                    hr->setVar("MS_LOG_LEVELS",(*ms_it)->log_levels());

                    hr->outSection("MS_LINE", tmp);
                    res += tmp;
                    (*ms_it)++;
                    cnt++;
                }
                delete ms_it;

                hr->outSection("MS_END", tmp);
                res += tmp;
            }

            if(show_qs)
            {
                hr->outSection("QS_BEGIN", tmp);
                res += tmp;

                _db_cl::_qservers_list::iterator *qs_it = _db_cl::_qservers_list().begin(tr);
                int cnt = 0;
                while(!qs_it->eof())
                {
                    hr->setVar("LINE_MOD", cnt % 2);
                    hr->setVar("QS_ID", (*qs_it)->id().value());
                    hr->setVar("QS_DNS_NAME", (*qs_it)->dns_name());
//                    hr->setVar("QS_ABOUT", (*qs_it)->about());
                    hr->setVar("QS_CONTROL_IP", (*qs_it)->control_ip());
                    hr->setVar("QS_CONTROL_PORT", (*qs_it)->control_port());
                    hr->setVar("QS_DATA_IP", (*qs_it)->data_ip());
                    hr->setVar("QS_DATA_PORT", (*qs_it)->data_port());
                    hr->setVar("QS_LOG_LEVELS", (*qs_it)->log_levels());

                    hr->outSection("QS_LINE",tmp);
                    res += tmp;
                    (*qs_it)++;
                    cnt++;
                }
                delete qs_it;

                hr->outSection("QS_END", tmp);
                res += tmp;
            }

            if(show_hs)
            {
                hr->outSection("HS_BEGIN", tmp);
                res += tmp;

                _db_cl::_hservers_list::iterator *hs_it = _db_cl::_hservers_list().begin(tr);
                int cnt = 0;
                while(!hs_it->eof())
                {
                    hr->setVar("LINE_MOD", cnt % 2);
                    hr->setVar("HS_ID", (*hs_it)->id().value());
                    hr->setVar("HS_DNS_NAME", (*hs_it)->dns_name());
                    hr->setVar("HS_CONTROL_IP", (*hs_it)->control_ip());
                    hr->setVar("HS_CONTROL_PORT", (*hs_it)->control_port());
                    hr->setVar("HS_DOC_ROOT", (*hs_it)->doc_root());
                    hr->setVar("HS_LOG_LEVELS", (*hs_it)->log_levels());

                    hr->outSection("HS_LINE", tmp);
                    res += tmp;
                    (*hs_it)++;
                    cnt++;
                }
                delete hs_it;

                hr->outSection("HS_END", tmp);
                res += tmp;
            }

            hr->outSection("END", tmp);
            res += tmp;

            tr->commit();
            delete tr;
        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code=0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------


