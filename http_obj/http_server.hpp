#ifndef __HTTP_OBJ_SERVER__
#define __HTTP_OBJ_SERVER__

#include <string>
#include "../common/db_obj/server.hpp"
#include "../session.hpp"

// --------------------------------------------------

class _http_mserver : public _client_object
{
    private:
    protected:
    public:

      _http_mserver(_client_session *session):_client_object(session,"mserver") {};
      virtual ~_http_mserver() {};

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res);
};

// --------------------------------------------------

class _http_hserver : public _client_object
{
    private:
    protected:
    public:

      _http_hserver(_client_session *session):_client_object(session,"hserver") {};
      virtual ~_http_hserver() {}; 

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res);
};

// --------------------------------------------------

class _http_qserver : public _client_object
{
    private:
    protected:
    public:

      _http_qserver(_client_session *session):_client_object(session,"qserver") {};
      virtual ~_http_qserver() {}; 

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res);
};

class _http_queue : public _client_object
{
    private:
    protected:
    public:

      _http_queue(_client_session *session):_client_object(session,"queue") {};
      virtual ~_http_queue() {};

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res);
};

class _http_queues_list : public _client_object
{
    private:
    protected:
    public:

      _http_queues_list(_client_session *session):_client_object(session,"queues_list") {};
      virtual ~_http_queues_list() {};

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res);
};

class _http_storage : public _client_object
{
    private:
    protected:
    public:

      _http_storage(_client_session *session) : _client_object(session, "storage") {};
      virtual ~_http_storage() {};

      virtual int cmd(std::string &act, list<std::string> &params, list<std::string> &values, std::string &res);
};

class _http_storages_list : public _client_object
{
    private:
    protected:
    public:

      _http_storages_list(_client_session *session) : _client_object(session, "storages_list") {};
      virtual ~_http_storages_list() {};

      virtual int cmd(std::string &act, list<std::string> &params, list<std::string> &values, std::string &res);
};

// --------------------------------------------------

class _http_servers_list : public _client_object
{
    private:
    protected:
    public:

      _http_servers_list(_client_session *session):_client_object(session,"servers_list") {}; 
      virtual ~_http_servers_list() {};

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &res);
};

#endif
