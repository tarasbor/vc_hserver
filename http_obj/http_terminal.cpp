#include "http_terminal.hpp"
#include <net_http.h>
#include <htmlrep.hpp>
#ifdef VC_ORACLE
    #include <obj_db5/base_oracle.hpp>
#endif
#ifdef VC_DB2
    #include <obj_db5/base_db2.hpp>
#endif

// --------------------------------------

_http_terminal::_http_terminal(_client_session *session) : _client_object(session, "terminal")
{
}

_http_terminal::~_http_terminal()
{
}

int _http_terminal::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;
    
    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "terminal.new")
    {
        hh->http_file(_session->root_dir(), "/terminal/terminal.html", res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
        hr->outSection("MAIN", res);
                
        e_code = 0;
    }
    else if(act == "terminal.show")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();
            
            _db_cl::_terminal* t = new _db_cl::_terminal(hh->req_get_param(params, values, "id"), tr);

            hh->http_file(_session->root_dir(), "/terminal/terminal.html", res);
            hr->setTemplateText(res.c_str());
            
            hr->setVar("ID", t->id().value());
            hr->setVar("DNS_NAME", t->dns_name());
            hr->setVar("VNAME", t->vname());
            hr->setVar("PLACE", t->place());
            hr->setVar("PRINTER_NAME", t->printer_name());
            hr->setVar("SCANNER_NAME", t->scanner_name());
            hr->setVar("VIDEO_ROTATE_ANGLE", t->video_rotate_angle());

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
                        
            hr->outSection("MAIN", res);
            
            delete t;
            
            tr->commit();
            delete tr;
        }        
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "terminal.apply")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();
                       
            _db_id id(hh->req_get_param(params, values, "id"));
            _db_cl::_terminal *t;
            t = id.is_set() ? new _db_cl::_terminal(id,tr) : new _db_cl::_terminal(tr);

            t->dns_name(hh->req_get_param(params, values, "dns_name"));
            t->vname(hh->req_get_param(params, values, "vname"));
            t->place(hh->req_get_param(params, values, "place"));
            t->printer_name(hh->req_get_param(params, values, "printer_name"));
            t->scanner_name(hh->req_get_param(params, values, "scanner_name"));
            t->video_rotate_angle(atoi(hh->req_get_param(params, values, "video_rotate_angle").c_str()));
                        
            delete t;

            tr->commit();
            delete tr;
            
            hh->http_file(_session->root_dir(), "/comctl/done.html", res);
        }
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }
        
        e_code = 0;
    }

    delete db;
    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

_http_terminals_list::_http_terminals_list(_client_session *session) : _client_object(session, "terminals_list")
{
}

_http_terminals_list::~_http_terminals_list()
{
}

int _http_terminals_list::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;
    std::string tmp;
    
    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "terminals_list.show")
    {
        try
        {
            hh->http_file(_session->root_dir(), "/terminal/terminals_list.html", res);
            hr->setTemplateText(res.c_str());

            hr->outSection("MAIN", res);
            
            _transaction* tr = db->transaction();
            tr->start();

            _db_cl::_terminals_list::iterator* tit = _db_cl::_terminals_list().begin(tr);
            int cnt;
            while(!tit->eof())
            {
                hr->setVar("LINE_MOD", cnt%2);
                hr->setVar("ID", (*tit)->id().value());
                hr->setVar("DNS_NAME", (*tit)->dns_name());
                hr->setVar("VNAME", (*tit)->vname());
                hr->setVar("PLACE", (*tit)->place());
                hr->setVar("PRINTER_NAME", (*tit)->printer_name());
                hr->setVar("SCANNER_NAME", (*tit)->scanner_name());
                hr->setVar("VIDEO_ROTATE_ANGLE", (*tit)->video_rotate_angle());

                hr->outSection("TERM_LINE", tmp);
                res += tmp;
                (*tit)++;
                cnt++;
            }
            delete tit;
            
            tr->commit();
            delete tr;

            hr->outSection("END", tmp);
            res += tmp;

        }        
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
   
    delete db;
    
    hh->release();
    hr->release();

    return e_code;
}

