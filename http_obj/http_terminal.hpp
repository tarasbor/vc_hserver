#ifndef __HTTP_OBJ_TERMINAL__
#define __HTTP_OBJ_TERMINAL__

#include <string>
#include "../common/db_obj/terminal.hpp"
#include "../session.hpp"

// --------------------------------------------------
// --------------------------------------------------
class _http_terminal:public _client_object
{
    private:
    protected:
    public:

      _http_terminal(_client_session *session); 
      virtual ~_http_terminal();

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez);
};

// --------------------------------------------------
// --------------------------------------------------
class _http_terminals_list:public _client_object
{
    private:
    protected:
    public:

      _http_terminals_list(_client_session *session); 
      virtual ~_http_terminals_list();

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez);
};

#endif
