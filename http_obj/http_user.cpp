#include <stdio.h>

#include <udefs.hpp>
#include <net_http.h>
#include <htmlrep.hpp>
#ifdef VC_ORACLE
    #include <obj_db5/base_oracle.hpp>
#endif
#ifdef VC_DB2
    #include <obj_db5/base_db2.hpp>
#endif
#include "http_user.hpp"

// --------------------------------------

_http_user::_http_user(_client_session* session) : _client_object(session, "user")
{
}

_http_user::~_http_user()
{
}

int _http_user::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();
    std::string tmp;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "user.new")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();
            hh->http_file(_session->root_dir(), "/user/user.html", res);
            hr->setTemplateText(res.c_str());
            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
            hr->outSection("MAIN", res);

            // user groups

            _db_cl::_ugroup_list::iterator* git = _db_cl::_ugroup_list().begin(tr);
            while(!git->eof())
            {
                hr->setVar("USER_GROUP_ID", (*git)->id().value());
                hr->setVar("USER_GROUP_NAME", (*git)->name());
                (*git)++;

                hr->outSection("g_line", tmp);
                res += tmp;
            }
            delete git;

            hr->outSection("g_end", tmp);
            res += tmp;

            // user queues

            _db_cl::_qs_list::iterator* qit = _db_cl::_qs_list().begin(tr);
            while(!qit->eof())
            {
                hr->setVar("USER_Q_ID", (*qit)->id().value());
                hr->setVar("USER_Q_VNAME", (*qit)->vname());
                (*qit)++;

                hr->outSection("q_line", tmp);
                res += tmp;
            }
            delete qit;

            // form footer

            hr->outSection("end", tmp);
            res += tmp;

            tr->commit();
            delete tr;
        }
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code=0;
    }
    else if(act == "user.show")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();

            _db_cl::_user* u = new _db_cl::_user(hh->req_get_param(params, values, "id"), tr);

            hh->http_file(_session->root_dir(), "/user/user.html", res);
            hr->setTemplateText(res.c_str());

            // user properties

            hr->setVar("ID", u->id().value());
            hr->setVar("LOGIN", u->login());
            hr->setVar("PASS", "****************");
            hr->setVar("PASS2", "****************");
            hr->setVar("F", u->fio_f());
            hr->setVar("I", u->fio_i());
            hr->setVar("O", u->fio_o());

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            hr->outSection("MAIN",res);

            // user groups

            list<_db_id> gids;
            _db_cl::_ugroup_list::iterator* git = u->groups();
            while(!git->eof())
            {
                gids.push_back((*git)->id());
                (*git)++;
            }
            delete git;

            git = _db_cl::_ugroup_list().begin(tr);
            while(!git->eof())
            {
                hr->setVar("USER_GROUP_ID", (*git)->id().value());
                hr->setVar("USER_GROUP_NAME", (*git)->name());
                hr->setVar("USER_IS_IN", "");

                list<_db_id>::iterator it = gids.begin();
                while(it != gids.end())
                {
                    if((*git)->id() == *it)
                    {
                        hr->setVar("USER_IS_IN", "checked");
                        break;
                    }
                    it++;
                }

                (*git)++;

                hr->outSection("g_line", tmp);
                res += tmp;
            }
            delete git;

            hr->outSection("g_end", tmp);
            res += tmp;

            // user queues

            list<_db_id> qids;
            _db_cl::_qs_list::iterator* qit = u->queues();
            while(!qit->eof())
            {
                qids.push_back((*qit)->id());
                (*qit)++;
            }
            delete qit;

            qit = _db_cl::_qs_list().begin(tr);
            while(!qit->eof())
            {
                hr->setVar("USER_Q_ID", (*qit)->id().value());
                hr->setVar("USER_Q_VNAME", (*qit)->vname());
                hr->setVar("USER_HAS", "");

                list<_db_id>::iterator it = qids.begin();
                while(it != qids.end())
                {
                    if((*qit)->id() == *it)
                    {
                        hr->setVar("USER_HAS", "checked");
                        break;
                    }
                    it++;
                }

                (*qit)++;

                hr->outSection("q_line", tmp);
                res += tmp;
            }
            delete qit;

            // form footer

            hr->outSection("end", tmp);
            res += tmp;

            delete u;

            tr->commit();
            delete tr;

        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "user.current_show")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();

            _db_cl::_user* u = new _db_cl::_user(_session->user_id(), tr);

            hh->http_file(_session->root_dir(), "/user/curr_user.html", res);
            hr->setTemplateText(res.c_str());

            hr->setVar("LOGIN",u->login());
            hr->setVar("F", u->fio_f());
            hr->setVar("I", u->fio_i());
            hr->setVar("O", u->fio_o());

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            hr->outSection("MAIN", res);

            delete u;

            tr->commit();
            delete tr;
        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "user.ch_pass_apply")
    {
        std::string old_pass = hh->req_get_param(params,values, "old_pass");
        std::string new_pass = hh->req_get_param(params,values, "new_pass");
        std::string new_pass2 = hh->req_get_param(params,values, "new_pass2");

        if(new_pass != new_pass2)
        {
            _http_db_error::out(_session->root_dir(), "/err/message.html", "Повторный ввод пароля неверен", res);
        }
        else
        {
            try
            {
                _transaction* tr = db->transaction();
                tr->start();

                _db_cl::_user* u = new _db_cl::_user(_session->user_id(), tr);

                if(u->pass() != old_pass)
                    _http_db_error::out(_session->root_dir(), "/err/message.html", "Старый пароль введён неверно",res);
                else
                {
                    u->pass(new_pass);
                    hh->http_file(_session->root_dir(), "/comctl/done.html", res);
                }

                delete u;

                tr->commit();
                delete tr;
            }
            catch(_db_error& e)
            {
                _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
            }
        }

        e_code = 0;
    }
    else if(act == "user.apply")
    {
        std::string pass = hh->req_get_param(params, values, "pass");
        std::string pass2 = hh->req_get_param(params, values, "pass2");

        if(pass != pass2)
        {
            _http_db_error::out(_session->root_dir(), "/err/message.html", "Повторный ввод пароля неверен", res);
        }
        else
        {
            try
            {
                _transaction* tr = db->transaction();
                tr->start();

                _db_id id(hh->req_get_param(params, values, "id"));
                bool exists = id.is_set();
                _db_cl::_user* u = exists ? new _db_cl::_user(id, tr) : new _db_cl::_user(tr);

                // user properties

                u->login(hh->req_get_param(params, values, "login"));
                if(pass != "****************")
                    u->pass(pass);
                u->fio_f(hh->req_get_param(params, values, "f"));
                u->fio_i(hh->req_get_param(params, values, "i"));
                u->fio_o(hh->req_get_param(params, values, "o"));

                // user groups

                _db_cl::_ugroup_list::iterator* git = _db_cl::_ugroup_list().begin(tr);
                while(!git->eof())
                {
                    char st[32];
                    #if defined(WINDOWS)
                        sprintf(st, "user_group_%I64i", (*git)->id().value());
                    #else
                        sprintf(st, "user_group_%Li", (*git)->id().value());
                    #endif

                    if(hh->req_get_param(params, values, st) == st)
                       u->add_to_group(*(*git));
                    else if(exists)
                       u->del_from_group(*(*git));

                    (*git)++;
                }
                delete git;

                // user queues

                _db_cl::_qs_list::iterator* qit = _db_cl::_qs_list().begin(tr);
                while(!qit->eof())
                {
                    char st[32];
                    #if defined(WINDOWS)
                        sprintf(st, "user_q_%I64i", (*qit)->id().value());
                    #else
                        sprintf(st, "user_q_%Li", (*qit)->id().value());
                    #endif
                    if(hh->req_get_param(params, values, st) == st)
                       u->assign_to_q(*(*qit), true, 0);
                    else if(exists)
                       u->deassign_from_q(*(*qit));

                    (*qit)++;
                }
                delete qit;

                //

                delete u;

                tr->commit();
                delete tr;

                hh->http_file(_session->root_dir(), "/comctl/done.html", res);
            }
            catch(_db_error& e)
            {
                _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
            }
        }

        e_code=0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

_http_user_list::_http_user_list(_client_session *session) : _client_object(session, "user_list")
{
}

_http_user_list::~_http_user_list()
{
}

int _http_user_list::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;
    std::string tmp;

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "user_list.show")
    {
        try
        {
            hh->http_file(_session->root_dir(), "/user/list.html", res);
            hr->setTemplateText(res.c_str());

            hr->outSection("MAIN", res);

            _transaction* tr = db->transaction();
            tr->start();

            _db_cl::_users_list::iterator* uit= _db_cl::_users_list().begin(tr);
            int cnt;
            while(!uit->eof())
            {
                hr->setVar("LINE_MOD", cnt%2);
                hr->setVar("USER_ID", (*uit)->id().value());
                hr->setVar("USER_LOGIN", (*uit)->login());
                hr->setVar("USER_F", (*uit)->fio_f());
                hr->setVar("USER_I", (*uit)->fio_i());
                hr->setVar("USER_O", (*uit)->fio_o());

                _db_cl::_ugroup_list::iterator *git=(*uit)->groups();
                tmp = "";
                while(!git->eof())
                {
                    if(tmp.size())
                        tmp += ",";
                    tmp += (*git)->name();
                    (*git)++;
                }
                delete git;
                hr->setVar("USER_GROUPS", tmp);

                hr->outSection("user_line", tmp);
                res += tmp;
                (*uit)++;
                cnt++;
            }
            delete uit;

            hr->outSection("end_users", tmp);
            res += tmp;

            // permissions list

            cnt = 0;
            _db_cl::_permission_list::iterator* pit = _db_cl::_permission_list().begin(tr);
            while(!pit->eof())
            {
                hr->setVar("LINE_MOD", cnt%2);
                hr->setVar("PERM_ID", (*pit)->id().value());
                hr->setVar("PERM_NAME", (*pit)->name());

                hr->outSection("perm_line", tmp);
                res += tmp;

                (*pit)++;
                cnt++;
            }
            delete pit;

            hr->outSection("perm_end", tmp);
            res += tmp;

            // groups list

            cnt = 0;
            _db_cl::_ugroup_list::iterator *git = _db_cl::_ugroup_list().begin(tr);
            while(!git->eof())
            {
                hr->setVar("LINE_MOD", cnt%2);
                hr->setVar("GROUP_ID", (*git)->id().value());
                hr->setVar("GROUP_NAME", (*git)->name());

                hr->outSection("group_line", tmp);
                res += tmp;

                (*git)++;
                cnt++;
            }
            delete git;

            tr->commit();
            delete tr;

            // form footer

            hr->outSection("end", tmp);
            res += tmp;
        }
        catch(_db_error &e)       { _http_db_error::out(_session->root_dir(),"/err/common.html",e.error_st(),res); }

        e_code=0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

_http_permission::_http_permission(_client_session *session) : _client_object(session, "permission")
{
}

_http_permission::~_http_permission()
{
}

int _http_permission::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    _db* db = NULL;
    _transaction* tr = NULL;
    _statement* st = NULL;

    _db_cl::_permission* p = NULL;

    if(act == "permission.new")
    {
        hh->http_file(_session->root_dir(), "/user/permission.html", res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
        hr->outSection("MAIN", res);

        e_code = 0;
    }
    else if(act == "permission.show")
    {
        try
        {
            #ifdef VC_ORACLE
                db = create_db_oracle();
            #endif
            #ifdef VC_DB2
                db = create_db_db2();
            #endif
            tr = db->transaction();
            tr->start();

            p = new _db_cl::_permission(hh->req_get_param(params, values, "id"), tr);

            hh->http_file(_session->root_dir(), "/user/permission.html", res);
            hr->setTemplateText(res.c_str());

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            hr->setVar("ID", p->id().value());
            hr->setVar("NAME", p->name());

            hr->outSection("MAIN", res);

            DEL(p);

            tr->commit();
            DEL(tr);
            DEL(db);
        }
        catch(_db_error& e)
        {
            if(p) delete p;
            if(st) { try { DEL(st); } catch(...) {} }
            if(tr) { try { tr->rollback(); DEL(tr); } catch(...) {} }
            if(db) { try { DEL(db); } catch(...) {} }
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "permission.apply")
    {
        try
        {
            #ifdef VC_ORACLE
                db = create_db_oracle();
            #endif
            #ifdef VC_DB2
                db = create_db_db2();
            #endif
            tr = db->transaction();
            tr->start();

            _db_id id(hh->req_get_param(params, values, "id"));
            p = id.is_set() ? new _db_cl::_permission(id, tr) : new _db_cl::_permission(tr);

            p->name(hh->req_get_param(params, values, "name"));

            DEL(p);

            tr->commit();
            DEL(tr);
            DEL(db);

            hh->http_file(_session->root_dir(), "/comctl/done.html", res);
        }
        catch(_db_error &e)
        {
            if(p) delete p;
            if(st) { try { DEL(st); } catch(...) {} }
            if(tr) { try { tr->rollback(); DEL(tr); } catch(...) {} }
            if(db) { try { DEL(db); } catch(...) {} }
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "permission.delete")
    {
        hh->http_file(_session->root_dir(), "/user/delete_permission.html", res);
        string tmp = string(res, res.find("\r\n\r\n"));
        hh->http_file(_session->root_dir(), "/comctl/yesno.html", res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
        hr->setVar("object_type", "permission");
        hr->setVar("action", "delete_commit");
        hr->setVar("id", hh->req_get_param(params, values, "id"));
        hr->setVarNoRep("question", tmp);

        hr->outSection("MAIN", res);
        e_code = 0;
    }
    else if(act == "permission.delete_commit")
    {
        try
        {
            _db_id id(hh->req_get_param(params, values, "id"));

            #ifdef VC_ORACLE
                db = create_db_oracle();
            #endif
            #ifdef VC_DB2
                db = create_db_db2();
            #endif

            tr = db->transaction();
            tr->start();
            st = tr->statement();

            st->prepare("delete from GL_PERMISSIONS where ID=:id<bigint>");
            st->push(id);
            st->execute();
            hh->http_file(_session->root_dir(), "/comctl/done.html", res);

            DEL(st);

            tr->commit();
            DEL(tr);
            DEL(db);
        }
        catch(_db_error& e)
        {
            if (st) { try { DEL(st); } catch(...) {} }
            if (tr) { try { tr->rollback(); DEL(tr); } catch(...) {} }
            if (db) { try { DEL(db); } catch(...) {} }
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

_http_group::_http_group(_client_session *session) : _client_object(session, "group")
{
}

_http_group::~_http_group()
{
}

int _http_group::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;

    _http_help* hh = create_http_help();
    _report* hr = create_html_report();
    std::string tmp;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "group.new")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();

            hh->http_file(_session->root_dir(), "/user/group.html", res);
            hr->setTemplateText(res.c_str());
            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
            hr->outSection("MAIN", res);

            // group rights

            _db_cl::_permission_list::iterator* pit = _db_cl::_permission_list().begin(tr);
            while(!pit->eof())
            {
                hr->setVar("RIGHT_ID", (*pit)->id().value());
                hr->setVar("RIGHT_NAME", (*pit)->name());
                (*pit)++;

                hr->outSection("g_right_line", tmp);
                res += tmp;
            }
            delete pit;

            hr->outSection("end", tmp);
            res += tmp;

            tr->commit();
            delete tr;
        }
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "group.show")
    {
        try
        {
            hh->http_file(_session->root_dir(), "/user/group.html", res);
            hr->setTemplateText(res.c_str());
            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");

            _transaction* tr = db->transaction();
            tr->start();

            // group properties

            _db_cl::_ugroup* g = new _db_cl::_ugroup(hh->req_get_param(params, values, "id"), tr);

            hr->setVar("ID", g->id().value());
            hr->setVar("NAME", g->name());

            hr->outSection("MAIN",res);

            // group rights

            _db_cl::_permission_list::iterator* pit = _db_cl::_permission_list().begin(tr);
            while(!pit->eof())
            {
                hr->setVar("PERM_ID", (*pit)->id().value());
                hr->setVar("PERM_NAME", (*pit)->name());

                hr->setVar("VALUE_y", "");
                hr->setVar("VALUE_n", "");
                hr->setVar("VALUE_str", "");
                hr->setVar("RIGHT_VALUE_STR", "");

                _db_cl::_ugroup::_right_list::iterator* grit = g->group_rights();
                while(!grit->eof())
                {
                    if((*pit)->id() == (*grit)->perm_id())
                    {
                        if((*grit)->cvalue() == "y")
                            hr->setVar("VALUE_y", "checked");
                        else
                        {
                            hr->setVar("VALUE_str", "checked");
                            hr->setVar("RIGHT_VALUE_STR", (*grit)->cvalue());
                        }
                        break;
                    }
                    (*grit)++;
                }
                if(grit->eof())
                    hr->setVar("VALUE_n", "checked");
                delete grit;

                (*pit)++;

                hr->outSection("g_right_line", tmp);
                res += tmp;
            }
            delete pit;

            hr->outSection("end", tmp);
            res += tmp;

            delete g;

            tr->commit();
            delete tr;
        }
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "group.apply")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();

            _db_id g_id(hh->req_get_param(params, values, "id"));
            bool exists = g_id.is_set();
            _db_cl::_ugroup *g = exists ? new _db_cl::_ugroup(g_id, tr) : new _db_cl::_ugroup(tr);

            // group properties

            g->name(hh->req_get_param(params, values, "name"));

            // group rights

            string val;
            _db_cl::_permission_list::iterator *pit = _db_cl::_permission_list().begin(tr);
            while(!pit->eof())
            {
                char idstr[32];
                #if defined(WINDOWS)
                    sprintf(idstr, "_%I64i", (*pit)->id().value());
                #else
                    sprintf(idstr, "_%Li", (*pit)->id().value());
                #endif

                val = hh->req_get_param(params, values, string("right_value") + idstr);

                if(val == string("y") + idstr)
                    g->add_permission(*(*pit), "y");
                else if(val == string("str") + idstr)
                {
                    val = hh->req_get_param(params, values, "RIGHT_VALUE_STR");
                    g->add_permission(*(*pit), val);
                }
                else if(exists) // val == "n" + idstr
                    g->del_permission(*(*pit));

                (*pit)++;
            }
            delete pit;

            delete g;

            tr->commit();
            delete tr;

            hh->http_file(_session->root_dir(), "/comctl/done.html", res);
        }
        catch(_db_error &e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}
