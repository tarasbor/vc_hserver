#ifndef __HTTP_OBJ_USER__
#define __HTTP_OBJ_USER__

#include <string>
#include "../common/db_obj/user.hpp"
#include "../session.hpp"

// --------------------------------------------------

class _http_user : public _client_object
{
    private:
    protected:
    public:

      _http_user(_client_session* session);
      virtual ~_http_user();

      virtual int cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res);
};

// --------------------------------------------------

class _http_user_list : public _client_object
{
    private:
    protected:
    public:

      _http_user_list(_client_session* session);
      virtual ~_http_user_list();

      virtual int cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res);
};

// --------------------------------------------------

class _http_permission : public _client_object
{
    private:
    protected:
    public:

      _http_permission(_client_session* session);
      virtual ~_http_permission();

      virtual int cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res);
};

// --------------------------------------------------

class _http_group:public _client_object
{
    private:
    protected:
    public:

      _http_group(_client_session* session);
      virtual ~_http_group();

      virtual int cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res);
};

#endif
