#include "http_workstation.hpp"
#include <net_http.h>
#include <htmlrep.hpp>
#ifdef VC_ORACLE
    #include <obj_db5/base_oracle.hpp>
#endif
#ifdef VC_DB2
    #include <obj_db5/base_db2.hpp>
#endif

// --------------------------------------

_http_workstation::_http_workstation(_client_session *session) : _client_object(session, "workstation")
{
}

_http_workstation::~_http_workstation()
{
}

int _http_workstation::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;
    
    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "workstation.new")
    {
        hh->http_file(_session->root_dir(), "/workstation/workstation.html", res);
        hr->setTemplateText(res.c_str());
        hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
        hr->outSection("MAIN", res);
                
        e_code = 0;
    }
    else if(act == "workstation.show")
    {
        try
        {
            _transaction* tr = db->transaction();
            tr->start();
            
            _db_cl::_workstation* w = new _db_cl::_workstation(hh->req_get_param(params, values, "id"), tr);

            hh->http_file(_session->root_dir(), "/workstation/workstation.html", res);
            hr->setTemplateText(res.c_str());
            
            hr->setVar("ID", w->id().value());
            hr->setVar("DNS_NAME", w->dns_name());
            hr->setVar("VNAME", w->vname());
            hr->setVar("PLACE", w->place());
            hr->setVar("WORK_DIR", w->wdir());

            hr->setVarNoRep("refresh_url", "/refresh?act=refresh");
                        
            hr->outSection("MAIN", res);
            
            delete w;
            
            tr->commit();
            delete tr;

        }        
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }
    else if(act == "workstation.apply")
    {
        try
        {
            _transaction *tr = db->transaction();
            tr->start();
                       
            _db_id id(hh->req_get_param(params, values, "id"));
            _db_cl::_workstation* w;
            w = id.is_set() ? new _db_cl::_workstation(id, tr) : new _db_cl::_workstation(tr);

            w->dns_name(hh->req_get_param(params, values, "dns_name"));
            w->vname(hh->req_get_param(params, values, "vname"));
            w->place(hh->req_get_param(params, values, "place"));
            w->wdir(hh->req_get_param(params, values, "work_dir"));
                        
            delete w;

            tr->commit();
            delete tr;
            
            hh->http_file(_session->root_dir(), "/comctl/done.html", res);
        }
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }
        
        e_code=0;
    }

    delete db;
    
    hh->release();
    hr->release();

    return e_code;
}

// --------------------------------------

_http_workstations_list::_http_workstations_list(_client_session *session) : _client_object(session, "workstations_list")
{
}

_http_workstations_list::~_http_workstations_list()
{
}

int _http_workstations_list::cmd(std::string& act, list<std::string>& params, list<std::string>& values, std::string& res)
{
    int e_code = -1;
    std::string tmp;
    
    _http_help* hh = create_http_help();
    _report* hr = create_html_report();

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif

    if(act == "workstations_list.show")
    {
        try
        {
            hh->http_file(_session->root_dir(),"/workstation/workstations_list.html", res);
            hr->setTemplateText(res.c_str());

            hr->outSection("MAIN", res);
            
            _transaction *tr = db->transaction();
            tr->start();

            _db_cl::_workstations_list::iterator* wit = _db_cl::_workstations_list().begin(tr);
            int cnt;
            while(!wit->eof())
            {
                hr->setVar("LINE_MOD", cnt%2);
                hr->setVar("ID", (*wit)->id().value());
                hr->setVar("DNS_NAME", (*wit)->dns_name());
                hr->setVar("VNAME", (*wit)->vname());
                hr->setVar("PLACE", (*wit)->place());
                hr->setVar("WORK_DIR", (*wit)->wdir());

                hr->outSection("WS_LINE", tmp);
                res += tmp;
                (*wit)++;
                cnt++;
            }
            delete wit;
            
            tr->commit();
            delete tr;

            hr->outSection("END", tmp);
            res += tmp;

        }        
        catch(_db_error& e)
        {
            _http_db_error::out(_session->root_dir(), "/err/common.html", e.error_st(), res);
        }

        e_code = 0;
    }

    delete db;

    hh->release();
    hr->release();

    return e_code;
}

