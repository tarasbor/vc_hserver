#ifndef __HTTP_OBJ_WORKSTATION__
#define __HTTP_OBJ_WORKSTATION__

#include <string>
#include "../common/db_obj/workstation.hpp"
#include "../session.hpp"

// --------------------------------------------------
// --------------------------------------------------
class _http_workstation:public _client_object
{
    private:
    protected:
    public:

      _http_workstation(_client_session *session); 
      virtual ~_http_workstation();

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez);
};

// --------------------------------------------------
// --------------------------------------------------
class _http_workstations_list:public _client_object
{
    private:
    protected:
    public:

      _http_workstations_list(_client_session *session); 
      virtual ~_http_workstations_list();

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez);
};

#endif
