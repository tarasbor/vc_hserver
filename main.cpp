#if defined(WINDOWS)
#include <windows.h>
    #if !defined(AS_APP)
    #define AS_SERVICE
    #endif
#else
    #define MAX_PATH        255
#endif

#include <error.hpp>
#include <log_file_writer.hpp>
#include <log.hpp>
#include <system_env.hpp>
#include <time.hpp>
#ifdef VC_ORACLE
    #include <obj_db5/base_oracle.hpp>
#endif
#ifdef VC_DB2
    #include <obj_db5/base_db2.hpp>
#endif
#include <text_help.hpp>

#include "../common/version.hpp"
#include "../common/user.hpp"
#include "../common/settings.hpp"

#include "gl_hserver.hpp"
#include "sheduler.hpp"
#include <config_parser.hpp>

#if defined(WINDOWS)
    HINSTANCE hAppInst;
    char CmdLine[MAX_PATH];

    SERVICE_STATUS glSS;
    SERVICE_STATUS_HANDLE glSS_H;
#else
#endif

#define LPROG(A) _log::_set_module_level("prog",(A))
_log log;

const char c_hserver_version[]={"0.4.4"};


class _config_file:public _config_parser_from_file
{
public:
	_config_file() {};
	virtual ~_config_file() {};

	virtual void parse_done() const;
};

#if defined(WINDOWS)
    int APIENTRY WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow);
    VOID WINAPI MainServiceStart(DWORD dwArgc,LPTSTR *lpszArgv);
    #if defined(AS_SERVICE)
        VOID WINAPI ServiceHandler(DWORD fdwControl);
    #endif
#else
    int main(int n,char *p[]);
#endif

int main_loop(int params_n,char *params[]);
int init();
int shutdown();

#if defined(WINDOWS)
// ----------------------------------------------------------
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
    hAppInst = hInstance;
    strncpy(CmdLine,lpCmdLine,MAX_PATH-1);

    // run only one copy
    SECURITY_ATTRIBUTES SA;
    SA.nLength=sizeof(SECURITY_ATTRIBUTES);
    SA.lpSecurityDescriptor=NULL;
    SA.bInheritHandle=TRUE;
    CreateMutex(&SA,TRUE, "_MT_VC_HSERVER_");
    if (GetLastError()==ERROR_ALREADY_EXISTS) return 1;

    #if defined(AS_SERVICE)
        SERVICE_TABLE_ENTRY   DispatchTable[] =
        {
            { TEXT("Video Consultant HTTP Server"), MainServiceStart},
            { NULL,NULL }
        };
        if (!StartServiceCtrlDispatcher( DispatchTable))
        {
            return GetLastError();
        }
    #else
        MainServiceStart(1,NULL);
    #endif

    return 0;
}

#if defined(AS_SERVICE)
VOID WINAPI ServiceHandler(DWORD fdwControl)
{
    if (fdwControl==SERVICE_CONTROL_STOP || fdwControl==SERVICE_CONTROL_SHUTDOWN)
    {
        _hserver_status().need_exit(true);
    }
    else if (fdwControl==SERVICE_CONTROL_INTERROGATE)
    {
        SetServiceStatus(glSS_H,&glSS);
    }
    else if (fdwControl==SERVICE_CONTROL_CONTINUE || fdwControl==SERVICE_CONTROL_PAUSE)
    {
        //?????
    }
}
#endif

// ----------------------------------------------------------
VOID WINAPI MainServiceStart(DWORD dwArgc,LPTSTR *lpszArgv)
{
    #ifndef AS_APP
        glSS.dwServiceType   = SERVICE_WIN32_OWN_PROCESS;
        glSS.dwCurrentState  = SERVICE_START_PENDING;
        glSS.dwWin32ExitCode = 0;
        glSS.dwServiceSpecificExitCode = 0;
        glSS.dwCheckPoint    = 0;
        glSS.dwWaitHint      = 0;

        glSS_H = RegisterServiceCtrlHandler("Video Consultant HTTP Server",ServiceHandler);
        if (!glSS_H)
        {
            return;
        }

        BOOL bres = SetServiceStatus(glSS_H,&glSS);
        if (!bres)
        {
            return;
        }
    #endif

    if (strlen(CmdLine))
    {
        _hserver_status().config_file_name(CmdLine);
    }
    else
    {
        _hserver_status().config_file_name(se_get_exe_path(hAppInst)+"vc_hserver.conf");
    }

    main_loop(0,NULL);

    return;
}

#else    // *nix

// ----------------------------------------------------------
int main(int n,char *p[])
{
    if (n==1) // no params
    {
        _hserver_status().config_file_name("/etc/vc/vc_hserver.conf");
    }
    else if (n==2)
    {
        _hserver_status().config_file_name(p[1]);
    }
    else
    {
        printf("hserver::too many params\nUsage: hserver [config_file_name]\n");
        return -1;
    }

    // TODO demonize

    return main_loop(n,p);
}

#endif

// ----------------------------------------------------------
int main_loop(int params_n,char *params[])
{
    bool first_init=true;
    _hserver_status().need_restart(true);

    while(1)
    {

        if (_hserver_status().need_exit()) break;
        if (_hserver_status().need_restart())
        {
            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_RUNNING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 0;
            glSS.dwControlsAccepted  = 0;
            SetServiceStatus(glSS_H,&glSS);
            #endif

            int err;
            if (!first_init)
            {
                log << LPROG(0) << "Restarting... Wait a moment... " << endl;
                Sleep(5000);

                err = shutdown();
                if (err)
                {
                    #if defined(WINDOWS) && defined(AS_SERVICE)
                    glSS.dwCurrentState  = SERVICE_STOPPED;
                    glSS.dwWin32ExitCode = err;
                    glSS.dwServiceSpecificExitCode = err;
                    SetServiceStatus(glSS_H,&glSS);
                    #endif

                    return err;
                }
            }

            err = init();
            if (err)
            {
                #if defined(WINDOWS) && defined (AS_SERVICE)
                glSS.dwCurrentState  = SERVICE_STOPPED;
                glSS.dwWin32ExitCode = err;
                glSS.dwServiceSpecificExitCode = err;
                SetServiceStatus(glSS_H,&glSS);
                #endif

                return err;
            }

            first_init=false;
            _hserver_status().need_restart(false);

            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_RUNNING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 0;
            glSS.dwControlsAccepted  = SERVICE_ACCEPT_STOP;
            SetServiceStatus(glSS_H,&glSS);
            #endif
        }

        Sleep(1000);

    }

    shutdown();

    #if defined(WINDOWS) && defined(AS_SERVICE)
    glSS.dwCurrentState  = SERVICE_STOPPED;
    glSS.dwWin32ExitCode = 0;
    glSS.dwServiceSpecificExitCode = 0;
    glSS.dwCheckPoint    = 0;
    SetServiceStatus(glSS_H,&glSS);
    #endif

    return 0;
}


// ----------------------------------------------------------
int init()
{
    try
    {
        #ifdef WINDOWS
            log.set_writer(new _log_file_writer(se_get_exe_path(hAppInst) + "vc_hserver.log"));
        #else
            log.set_writer(new _log_file_writer("/var/log/vc/vc_hserver.log"));
        #endif

        log.max_level4module("prog", 15);

        log << LPROG(0) << "___________________________________________________________________________________________________" << endl;

        log << LPROG(0) << platform_name << " HTTP Server starting... OK" << endl;
        log << LPROG(0) << "Version: " << c_hserver_version << "  vccommon: " << version << "  BDT: " << build_date << " " << build_time << endl;
//        log << "Flags are:";

        #ifdef AS_APP
            log << "Starting as application... " << endl;
        #else
            log << "Starting as service... " << endl;
        #endif

        #ifdef TCP_SERVER_TO_REINIT
            log << "TCP_SERVER_TO_REINIT = " << TCP_SERVER_TO_REINIT << endl;
        #else
        #endif

        log << "Start log...OK" << endl;

        // -------------------------------------------------------------
        log << LPROG(0) << "Loading local settings... ";
        _config_file *cf = new _config_file;
		cf->load(_hserver_status().config_file_name());
		delete cf;
        log << LPROG(0) << "OK" << endl;

        while(1)
        {
            try
            {
                log << LPROG(0) << "Trying connect to main server... " << std::flush;
                _hserver_status().net_control(create_simple_net_cmd_client());
                _hserver_status().net_control()->connect(_tcp_adress(_hserver_status().main_server_name()));
                log << LPROG(0) << "OK" << endl;
            }
            catch(_nerror &e)
            {
                log << LPROG(0) << "error " << e.err_st_full() << std::endl;
                log << LPROG(0) << "Wait 5 seconds & try again... " << std::endl;
                Sleep(5000);
                continue;
            }

            break;
        }

        log << LPROG(0) << "Checking main server status..." << std::flush;
        while(1)
        {
            std::string server_status;
            _hserver_status().net_control()->cmd("get_server_run_status", server_status);

//            if(server_status.compare("+ok: online;\r\n"))
            if(server_status[0] == '+') break;

            Sleep(1000);
        }
        log << LPROG(0) << "OK - main server is on." << endl;

        log << LPROG(0) << "Logging to main server..." << endl;
        std::string res;
        _hserver_status().net_control()->cmd("login hserver hserver", res);
        if(res[0] != '+')
            throw(_error(0,std::string("Can not login\n")+res.c_str()));
        _hserver_status().net_control()->cmd("get_global_settings", res);
        if(res[0] != '+')
            throw(_error(0,std::string("Can't retrieve settings from server ") + _hserver_status().main_server_name().c_str() + "\n" + res.c_str()));

        _settings::_global().load_from_text(res);

        // -------------------------------
        log << LPROG(0) << "Obtaining local dns name & local http server settings..." << endl;
        _settings::_system::_http_servers::const_iterator hs = _settings::_global().get()->get_local_http_server();
        log << LPROG(0) << "DNS NAME = " << (*hs).second.dns_name << endl;

        // NORMAL LOG LEVEL INIT
        int pn=0;
        while(1)
        {
            string pv=_text_help::get_field_from_st((*hs).second.log_levels, ";, ",pn);
        	if (pv.empty()) break;

        	string par=_text_help::get_field_from_st(pv,':',0);
        	string val=_text_help::get_field_from_st(pv,':',1);

        	_log().max_level4module(par,atoi(val.c_str()));

        	pn++;
        }

        // OFF time sink - it is not good idea to do it so
        /*
        if ((*hs).second.dns_name!=_text_help::get_field_from_st(_hserver_status().main_server_name(),':',0))
        {
            log << LPROG(0) << "Syncing date and time... " << endl;
            _hserver_status().net_control()->cmd(_hserver_status().main_server_name(), "get_time", res);
            if(res[0]!='+')
                THROW_EXCEPTION(std::string("Can not get time from main server\n") + res.c_str());

            std::string time_st;
            int pos=res.find("time = ");
            if (pos!=std::string::npos)
            {
                pos+=7;
                int pos2=res.find(";",pos);
                time_st = std::string(res,pos,pos2-pos);

                _time t;
                t.str_to_stamp(time_st.c_str());

                _time::_ys ys = t.split();

                #if defined(WINDOWS)
                    SYSTEMTIME tm;
                    tm.wYear = ys.y;
                    tm.wMonth = ys.m;
                    tm.wDay = ys.d;
                    tm.wHour = ys.h;
                    tm.wMinute = ys.n;
                    tm.wSecond = ys.s;
                    tm.wMilliseconds = ys.ms;

                    if(!SetLocalTime(&tm))
//                      throw(_error(GetLastError(),std::string("Can not set local time.")));
;//TODO                        THROW_EXCEPTION_N("Can not set local time.", GetLastError());
                #else
                    // TODO - time sync
                #endif
            }
            else
                THROW_EXCEPTION(std::string("Wrong answer from main server at get_time command\n")+res.c_str());

            log << LPROG(0) << "OK" << endl;
        }
        */

        log << LPROG(0) << "Connecting database... " << endl;
        #ifdef VC_ORACLE
            _hserver_status().database(create_db_oracle());//new _db((*hs).second.max_db_connections));
        #endif
        #ifdef VC_DB2
            _hserver_status().database(create_db_db2());//new _db((*hs).second.max_db_connections));
        #endif
        int con_cnt=0;
        bool error_connect=false;
        while(con_cnt<3)
        {
            try
            {
                _hserver_status().database()->connect(_settings::_global().get()->db_info.dsn,
                                                      _settings::_global().get()->db_info.user,
                                                      _settings::_global().get()->db_info.pass);

                /*
                // we must create & delete empty transaction in main thread to:
                // 1. real check db connection
                // 2. alloc some mem ? (if no transaction in main thread - childs thread say "stack overflow" then transaction is created
                _transaction *tr;
                tr = _hserver_status().database()->transaction();
                tr->start();
                tr->commit();
                delete tr;
                */
            }
            catch(_db_error &e)
            {
                log << LPROG(0) << "Connect to db failed: " << e.error_st() << endl << "Will try in 20sec." << endl;
                con_cnt++;
                Sleep(20000);
                error_connect=true;
            }
            if(!error_connect)
                break;
        }

        if (error_connect)
            THROW_EXCEPTION("Database server down or wrong db settings");

        log << LPROG(0) << "OK" << endl;

        log << LPROG(0) << "Starting session manager... " << endl << "Use doc path:" << endl << (*hs).second.doc_root << endl;
        _hserver_status().session_manager(new _session_manager((*hs).second.doc_root.c_str(),(*hs).second.doc_root.c_str()));
        log << LPROG(0) << "OK" << endl;


        log << LPROG(0) << "Starting HTTP server... ";
        _hserver_status().http_server(create_http_server());
        _hserver_status().http_server()->set_processor(_hserver_status().session_manager());
        _hserver_status().http_server()->online(_tcp_adress((*hs).second.control_ip,(*hs).second.control_port));
        log << LPROG(0) << "OK" << endl;
    }
    catch(_rdg_exception &e)
    {
        log << LPROG(0) << "ERROR: " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
        #if defined(WINDOWS)
            #if defined(AS_APP)
                string caption(platform_name);
                caption += " ";
                caption += version;
                caption += " - hserver";
                MessageBox(NULL, e.description().c_str(), caption.c_str(), MB_OK);
            #endif
        #else
            printf("mserver::init error:\n%s\n", e.description().c_str());
        #endif

        return -1;
    }
    catch(_error &e)
    {
        log << LPROG(0) << "ERROR: " << e.str() << endl;
        #if defined(WINDOWS)
            #if defined(AS_APP)
                string caption(platform_name);
                caption += " ";
                caption += version;
                caption += " - hserver";
                MessageBox(NULL, e.str().c_str(), caption.c_str(), MB_OK);
            #endif
        #else
            printf("mserver::init error:\n%s\n", e.str().c_str());
        #endif

        return -1;
    }

    return 0;
}

// --------------------------------------------------
int shutdown()
{
    log << LPROG(0) << "Shutdown start..." << endl;

    try
    {
        if (_hserver_status().http_server())
        {
            log << LPROG(0) << "Shutting down HTTP server... ";
            _hserver_status().http_server()->shutdown();
            _hserver_status().http_server(NULL);
            log << LPROG(0) << "OK" << endl;

            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_STOP_PENDING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 1;
            SetServiceStatus(glSS_H,&glSS);
            #endif
        }

        if (_hserver_status().session_manager())
        {
            log << LPROG(0) << "Shutdown session manager... ";
            delete _hserver_status().session_manager();
            _hserver_status().session_manager(NULL);
            log << LPROG(0) << "OK" << endl;

            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_STOP_PENDING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 2;
            SetServiceStatus(glSS_H,&glSS);
            #endif
        }

        if (_hserver_status().database())
        {
            log << LPROG(0) << "Disconnecting from db... ";
            _hserver_status().database()->disconnect();
            delete _hserver_status().database();
            _hserver_status().database(NULL);
            log << LPROG(0) << "OK" << endl;

            #if defined(WINDOWS) && defined(AS_SERVICE)
            glSS.dwCurrentState  = SERVICE_STOP_PENDING;
            glSS.dwWin32ExitCode = 0;
            glSS.dwServiceSpecificExitCode = 0;
            glSS.dwCheckPoint    = 3;
            SetServiceStatus(glSS_H,&glSS);
            #endif
        }

        log << LPROG(0) << "Server stopped." << endl;

        #if defined(WINDOWS) && defined(AS_SERVICE)
        glSS.dwCurrentState  = SERVICE_STOPPED;
        glSS.dwWin32ExitCode = 0;
        glSS.dwServiceSpecificExitCode = 0;
        glSS.dwCheckPoint    = 0;
        SetServiceStatus(glSS_H,&glSS);
        #endif
    }
    catch(_error &e)
    {
        log << LPROG(0) << "Error while shutdown: " << e.str() << endl;

        #if defined(WINDOWS) && defined(AS_SERVICE)
        glSS.dwCurrentState  = SERVICE_STOPPED;
        glSS.dwWin32ExitCode = e.num();
        glSS.dwServiceSpecificExitCode = e.num();
        glSS.dwCheckPoint    = 0;
        SetServiceStatus(glSS_H,&glSS);
        #endif
    }

    return 0;
}

// ----------------------------------------------
void load_local_set(const char *file_name)
{
        ifstream f;
        f.open(file_name,ios::in);
        if (!f)
        {
            char tmp[MAX_PATH+128];
            sprintf(tmp, "Can not open file with local configuration (%s).",file_name);
            THROW_EXCEPTION(tmp);
        }

        f.seekg(0,ios::end);
        int size = f.tellg();
        f.seekg(0,ios::beg);

        char *tmp = new char [size];

        f.read(tmp,size);
        f.close();

        std::string s=tmp;
        delete tmp;

        std::string server=_text_help::get_field_from_st(s, " :\r\n",0);
        std::string port=_text_help::get_field_from_st(s, " :\r\n",1);

        if (server=="" || port=="")
            THROW_EXCEPTION("Main server name & main server conrtol port are not defined in local configuration file");

        _hserver_status().main_server_name(server+':'+port);
}

// ----------------------------------------------
void _config_file::parse_done() const
{
    _config_parser::_params_map::const_iterator it = cm_params_map.begin();
    while(it!=cm_params_map.end())
    {
        if(it->first=="main_server")
            _hserver_status().main_server_name(it->second);
        else
            log << LPROG(0) << "Warning: unknown param '" << it->first << "'" << endl;

		it++;
    }

    if(_hserver_status().main_server_name().empty())
        THROW_EXCEPTION("Main server name is not defined in local configuration file");
}


