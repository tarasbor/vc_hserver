#include "session.hpp"
#include "http_obj/http_user.hpp"
#include "http_obj/http_terminal.hpp"
#include "http_obj/http_workstation.hpp"
#include "http_obj/http_constant.hpp"
#include "http_obj/http_server.hpp"
#include "http_obj/http_report.hpp"
#include "http_obj/http_calendar.hpp"
/*#include "http_obj/http_img_s.hpp"
#include "http_obj/http_document.hpp"
#include "http_obj/http_obj_cat.hpp"
#include "http_obj/http_work_mode.hpp"
#include "http_obj/http_access_point.hpp"
#include "http_obj/http_access_rule.hpp"
#include "http_obj/http_object.hpp"
#include "http_obj/http_ogroup.hpp"
#include "http_obj/http_key.hpp"
*/
#include "log.hpp"
#include <net_http.h>
#include <signal.h>
//#include <direct.h>
#include <htmlrep.hpp>
#ifdef VC_ORACLE
    #include <obj_db5/base_oracle.hpp>
#endif
#ifdef VC_DB2
    #include <obj_db5/base_db2.hpp>
#endif

#define MLOG(A) _log() << _log::_set_module_level("prog",(A))

// -------------------------------------------------------------

_client_session::_client_session():_id(time(NULL)),_last_need(_id),_rnd(0),obj(NULL),_old_req("")
{
}

_client_session::~_client_session()
{
    if(obj)
        delete obj;
}

void _client_session::save_return_fields(const std::string &line)
{
    ret_fields.clear();

    std::string::size_type pos_b=0;
    while(1)
    {
        std::string::size_type pos_e=line.find(',',pos_b+1);
        if (pos_e==std::string::npos) pos_e=line.length();
        ret_fields.push_back(std::string(line,pos_b,pos_e-pos_b));
        pos_b=pos_e+1;
        if (pos_b>=line.length()) break;
    }

    /*list<std::string>::iterator it=ret_fields.begin();
    while(it!=ret_fields.end())
    {
        cout << "ret_field :: " << *it << endl << flush;
        it++;
    }*/
}

void _client_session::access_cut(std::string &rez)
{
    int last_cut_pos=0;
    while(1)
    {
        std::string::size_type pos_a_begin = rez.find("<!--#access=", last_cut_pos);
        if(pos_a_begin == std::string::npos)
            break;
        last_cut_pos = pos_a_begin;
        std::string::size_type pos_a_end = rez.find("<!-- } -->", pos_a_begin) + 10;
        std::string::size_type g_pos = pos_a_begin + 12;
        bool its_end = false;
        bool enable_item = false;
        while(!its_end)
        {
            std::string::size_type g_end = rez.find(',',g_pos);
            std::string::size_type g_end_sp = rez.find(' ',g_pos);
            if(g_end_sp < g_end || g_end == std::string::npos)
            {
                g_end = g_end_sp;
                its_end = true;
            }

            list<std::string>::iterator gnit = _user_groups.begin();
            while(gnit != _user_groups.end())
            {
                //cout << (*gnit) << " " << rez.substr(g_pos,g_end-g_pos) << endl;
                if((*gnit) == rez.substr(g_pos, g_end-g_pos))
                {
                    enable_item = true;
                    break;
                }
                gnit++;
            }
            if (enable_item) break;

            g_pos = g_end + 1;
        }

        if(!enable_item)
            rez.replace(pos_a_begin, pos_a_end - pos_a_begin, "");
        else
        {
            rez.replace(pos_a_end - 10, 10, "");
            int pos = rez.find("-->", pos_a_begin);
            pos += 3;
            rez.replace(pos_a_begin, pos - pos_a_begin, "");
        }
    }
}

void _client_session::add_sid_rid(std::string &rez)
{
    srand(time(NULL));
    _rnd=rand()*time(NULL);
    if (_rnd<0) _rnd=-_rnd;

    char tmp[256];
    sprintf(tmp,"&sid=%i&srnd=%i",_id,_rnd);

    //cout << "a href=\"/cmd? replace begin " << GetTickCount() << endl << flush;

    std::string::size_type pos=0;
    while(1)
    {
        pos=rez.find("/cmd?act=",pos);
        if (pos==std::string::npos) break;
        std::string::size_type pos_s=rez.find_first_of("\"'",pos+1);
        if (pos_s==std::string::npos) pos_s=rez.length();
        rez.insert(pos_s,tmp);
        pos=pos_s+1;
    }

    //cout << "'/cmd?act replace begin " << GetTickCount() << endl << flush;

    pos=0;
    while(1)
    {
        pos=rez.find("'/refresh",pos);
        if (pos==std::string::npos) break;
        std::string::size_type pos_s=rez.find('\'',pos+2);
        if (pos_s==std::string::npos) pos_s=rez.length();
        //rez.replace(pos_s,0,tmp);
        rez.insert(pos_s,tmp);
        pos=pos_s+1;
    }

    pos=0;
    while(1)
    {
        pos=rez.find("/tpl?file=",pos);
        if (pos==std::string::npos) break;
        std::string::size_type pos_s=rez.find_first_of("\"'",pos+1);
        if (pos_s==std::string::npos) pos_s=rez.length();
        rez.insert(pos_s,tmp);
        pos=pos_s+1;
    }

    //cout << "<form action=\"/cmd replace begin " << GetTickCount() << endl << flush;
    pos=0;
    sprintf(tmp,"<input type=\"hidden\" name=\"sid\" value=\"%i\"><input type=\"hidden\" name=\"srnd\" value=\"%i\">",_id,_rnd);
    while(1)
    {
        pos=rez.find("<form action=\"/cmd",pos);
        if (pos==std::string::npos) break;
        std::string::size_type pos_s=rez.find('>',pos+10);
        if (pos_s==std::string::npos) pos_s=rez.length(); else pos_s++;
        rez.replace(pos_s,0,tmp);
        pos=pos_s+1;
    }

    //cout << "access_cut begin " << GetTickCount() << endl << flush;
    access_cut(rez);

    //cout << "all done " << GetTickCount() << endl << flush;
}

void _client_session::cmd(list<std::string> &params,list<std::string> &values,std::string &rez)
{
    _http_help *hh=create_http_help();

    std::string act=hh->req_get_param(params,values,"act");

    //cout << "act is " << act << endl << flush;

    if (act=="")
    {
        hh->http_file(_root_dir,"/session_err.html",rez);
        return;
    }

    std::string rfield_st=hh->req_get_param(params,values,"sys_ret_fields");
    if (!rfield_st.empty()) save_return_fields(rfield_st);

    if (act=="auth")
    {
        hh->http_file(_root_dir,"/hello.html",rez);
        add_sid_rid(rez);

        _last_need=time(NULL);
        hh->release();

        //cm_in_use=false;
        return;
    }

    if (act=="sys_return")
    {
        std::string tmp;
        hh->http_file(_root_dir,"/comctl/return.html",rez);
        _report *hr=create_html_report();

        hr->setTemplateText(rez.c_str());

        hr->outSection("MAIN",rez);

        list<std::string>::iterator it=ret_fields.begin();
        int i=-1;
        while(it!=ret_fields.end())
        {
            if
                (i==-1) hr->setVarNoRep("RET_FORM",(*it));
            else
            {
                char tmp_st[16];
                hr->setVarNoRep("RET_FIELD",(*it));
                sprintf(tmp_st,"%i",i);
                hr->setVarNoRep("RET_VALUE",hh->req_get_param(params,values,tmp_st));
                hr->outSection("LINE",tmp);
                rez+=tmp;
            }

            it++;
            i++;
        }

        hr->outSection("END",tmp);
        rez+=tmp;

        add_sid_rid(rez);

        hr->release();
        hh->release();

        _last_need=time(NULL);
        //cm_in_use=false;
        return;
	}

	if (obj)
    {
        if (act.find(obj->class_name())!=0)
        {
            delete obj;
            obj=NULL;
        }
    }

    if (!obj)
    {
        if (act.find("servers_list.")==0)          obj = new _http_servers_list(this);

        else if (act.find("mserver.")==0)               obj = new _http_mserver(this);

        else if (act.find("hserver.")==0)               obj = new _http_hserver(this);

        else if (act.find("qserver.")==0)               obj = new _http_qserver(this);
        else if (act.find("queues_list.")==0)           obj = new _http_queues_list(this);
        else if (act.find("queue.")==0)                 obj = new _http_queue(this);
        else if (act.find("storages_list.")==0)         obj = new _http_storages_list(this);
        else if (act.find("storage.")==0)               obj = new _http_storage(this);

        else if (act.find("workstations_list.")==0)     obj = new _http_workstations_list(this);
        else if (act.find("workstation.")==0)           obj = new _http_workstation(this);

        else if (act.find("terminals_list.")==0)        obj = new _http_terminals_list(this);
        else if (act.find("terminal.")==0)              obj = new _http_terminal(this);

        else if (act.find("constants_list.")==0)        obj = new _http_constants_list(this);
        else if (act.find("constant.")==0)              obj = new _http_constant(this);

        else if (act.find("user.")==0)                  obj = new _http_user(this);
        else if (act.find("user_list.")==0)             obj = new _http_user_list(this);
        else if (act.find("group.")==0)                 obj = new _http_group(this);
        else if (act.find("permission.")==0)            obj = new _http_permission(this);

        else if (act.find("report.")==0)                obj = new _http_report(this);

        else if (act.find("calendar.")==0)              obj = new _http_calendar(this);
    }

    if (obj)
    {
        if (obj->cmd(act,params,values,rez)==-1) hh->http_file(_root_dir,"/command_err.html",rez);
    }
    else
    {
        hh->http_file(_root_dir,"/command_err.html",rez);
    }

    hh->release();

    //if(act.find("small_img.") == std::string::npos && act.find("document.download") == std::string::npos)
    add_sid_rid(rez);

    _last_need=time(NULL);
}


// -------------------------------------------------------------
// -------------------------------------------------------------

// -------------------------------------------------------------
_session_manager::_session_manager(const char *root_dir,const char *lib_dir):_root_dir(root_dir),_lib_dir(lib_dir)
{
    /*list<std::string> libs;

    if (_lib_dir[_lib_dir.length()-1]!='\\') _lib_dir+='\\';

    std::string out="    Find libs: ";

    DIR *dir;
    dir=opendir(lib_dir);
    if (dir)
    {
        dirent *de;
        while((de=readdir(dir))!=NULL)
        {
            if (strstr(de->d_name,".dll"))
            {
                libs.push_back(de->d_name);
                out+=de->d_name; out+=',';
            }
        }
        closedir(dir);
    }

    cout << endl << out << endl;
    _log().out(out,1,0);

    list<std::string>::iterator it=libs.begin();
    for(;it!=libs.end();it++)
    {
        std::string lib_name=_lib_dir+*it;
        HINSTANCE lib_inst;

        lib_inst=LoadLibrary(lib_name.c_str());
        if (!lib_inst)
        {
            _log().out("    Can't load lib : "+*it+". Skipping.",1,0);
            cout << "    Can't load lib : "+*it+". Skipping." << endl;
            continue;
        }

        _lib_object_class::get_names_f fun = (_lib_object_class::get_names_f) GetProcAddress(lib_inst,"session_http_objects_names");
        if (!fun)
        {
            _log().out("    Lib "+*it+" is't 'right' library - no names proc found. Skipping.",1,0);
            cout << "    Lib "+*it+" is't 'right' library - no names proc found. Skipping." << endl;
            FreeLibrary(lib_inst);
            continue;
        }
    }*/

}

// -------------------------------------------------------------
_session_manager::~_session_manager()
{
    list<_client_session *>::iterator it=slist.begin();
    for(;it!=slist.end();it++) delete (*it);
}

// -------------------------------------------------------------
bool _session_manager::process(std::string &req,std::string &rez,_net_server_connection *ns)
{
    //cout << req << endl;

    _http_help *hh=create_http_help();

    std::string method;
    std::string url;
    std::string ver;

    hh->req_get_muv(req,method,url,ver);

    MLOG(0) << "income request: \"" << url << "\"; sid: 1" << endl;
    //cout << "^---  income request - \""+url+"\"" << endl << flush;

    //cout << req << endl << flush;

    list<std::string> params;
    list<std::string> values;
    hh->req_params(req,params,values);

    /*
    list<std::string>::iterator pit=params.begin();
    list<std::string>::iterator vit=values.begin();
    while(pit!=params.end())
    {
        _log().out(std::string("       ")+(*pit)+"="+(*vit),1,15);
        pit++;
        vit++;
    }
    */


    if      (url=="/")
    {
        hh->http_file(_root_dir,"/index.html",rez);
    }
    else if (url.find("/cmd")==0)
    {
      if (hh->req_get_param(params,values,"act")=="auth")
      {
          std::string uname=hh->req_get_param(params,values,"username");
          std::string upass=hh->req_get_param(params,values,"userpass");
          auth(req,rez,uname,upass);
      }
      else if (hh->req_get_param(params,values,"act")=="auth_do")
      {
          std::string uname=hh->req_get_param(params,values,"username");
          std::string upass=hh->req_get_param(params,values,"userpass");
          auth(req,rez,uname,upass);
      }
      else
      {
          std::string st=hh->req_get_param(params,values,"sid");
          int sid=atoi(st.c_str());

          list<_client_session *>::iterator it=slist.begin();
          for(;it!=slist.end();it++)
              if ((*it)->id()==sid) break;

          if (it==slist.end())
          {
              MLOG(0) << "No such session ID; sid: " << sid << endl;
              hh->http_file(_root_dir,"/session_err.html",rez);
          }
          else if ((1==1) || (*it)->rnd()==atoi(hh->req_get_param(params,values,"srnd").c_str()))
          {
              if (hh->req_get_param(params,values,"sys_ns")=="")
              {
                 (*it)->cmd(params,values,rez);
                 if (hh->req_get_param(params,values,"update_refresh")=="false"); else (*it)->set_old_req(req);
              }
              else
              {
                  _client_session *cs=new_session(params,values,(*it)->user_id(),(*it)->id(),(*it)->user_groups());
                  if (hh->req_get_param(params,values,"update_refresh")=="false"); else cs->set_old_req(req);
                  cs->cmd(params,values,rez);
              }
          }
          else
          {
              MLOG(0) << "No right rnd for this session; sid: " << sid << endl;
              hh->http_file(_root_dir,"/session_err.html",rez);
          }
      }

    }
    else if (url.find("/tpl")==0)
    {
        bool need_session=true;
        if (url.find("/tpl?file=/auth_do.html&user_name=")!=std::string::npos) need_session=false;

        list<_client_session *>::iterator it=slist.end();
        if (need_session)
        {
            std::string st=hh->req_get_param(params,values,"sid");
            int sid=atoi(st.c_str());

            it=slist.begin();
            for(;it!=slist.end();it++)
                if ((*it)->id()==sid) break;

            if (it==slist.end())
            {
                MLOG(0) << "No such session ID; sid: " << sid << endl;
                hh->http_file(_root_dir,"/session_err.html",rez);
            }
        }

        if (!need_session || it!=slist.end())
        {
            hh->http_file(_root_dir,hh->req_get_param(params,values,"file"),rez);

            _report *hr=create_html_report();
            hr->setTemplateText(rez.c_str());

            list<std::string>::iterator pit=params.begin();
            list<std::string>::iterator vit=values.begin();
            while(pit!=params.end())
            {
                hr->setVarNoRep((*pit).c_str(),(*vit));
                pit++;
                vit++;
            }

            //hr->setVarNoRep("refresh_url","/refresh?act=refresh");

            hr->outSection("MAIN",rez);
            hr->release();

            if (need_session) (*it)->add_sid_rid(rez);
        }
    }
    else if (url.find("/refresh")==0)
    {
        int sid=atoi(hh->req_get_param(params,values,"sid").c_str());

        list<_client_session *>::iterator it=slist.begin();
        for(;it!=slist.end();it++)
           if ((*it)->id()==sid) break;

        if (it==slist.end())
        {
            MLOG(0) << "No such session ID; sid: " << sid << endl;
            hh->http_file(_root_dir,"/session_err.html",rez);
        }
        else
        {
            int p_sid=(*it)->parent_id();

            it=slist.begin();
            for(;it!=slist.end();it++)
               if ((*it)->id()==p_sid) break;

            if (it==slist.end())
            {
                hh->http_file(_root_dir,"/session_err.html",rez);
                MLOG(0) << "No right rnd for this session; sid: " << sid << endl;
            }
            else
            {
                req=(*it)->old_req();
                hh->req_params(req,params,values);
                (*it)->cmd(params,values,rez);
            }
        }
    }
    else if (url.find("/restart")==0)
    {
        raise(SIGINT);
        Sleep(10000);
        abort();
    }
    else if (hh->its_file(url)) hh->http_file(_root_dir,url,rez);
    else hh->http_not_found(rez);

    hh->update_content_len(rez);

    hh->release();

    //cout << rez << endl;

    return true;
}


// -------------------------------------------------------------
void _session_manager::auth(std::string &req,std::string &rez,std::string &uname,std::string &upass)
{
    _http_help *hh=create_http_help();

    _db_id user_id;
    list<std::string> user_groups;

    MLOG(0) << "Auth try... user: " << uname << "; sid: 1" << endl;

    #ifdef VC_ORACLE
        _db* db = create_db_oracle();
    #endif
    #ifdef VC_DB2
        _db* db = create_db_db2();
    #endif
    _transaction *tr = NULL;

    try
    {
        tr = db->transaction();
        tr->start();

        bool find=false;
        _db_cl::_users_list::_name_eq neq_cond(uname);
        _db_cl::_users_list::iterator *uit = _db_cl::_users_list().begin(tr,&neq_cond);
        if (!uit->eof()) find=true;

        if (!find || (find && upass!=(*uit)->pass()))
        {
            delete uit;

            tr->commit();
            delete tr;

            hh->http_file(_root_dir,"/auth_err.html",rez);
            hh->release();

            MLOG(0) << "Unable to auth user: " << uname << " (wrong pass); sid: 1" << endl;

            return;
        }

        user_id=(*uit)->id();

        _db_cl::_ugroup_list::iterator *git=(*uit)->groups();
        while(!git->eof())
        {
            user_groups.push_back((*git)->name());
            (*git)++;
        }
        delete git;

        delete uit;

        tr->commit();
        delete tr;

        MLOG(0) << " Login - done. sid: 1" << endl;
    }
    catch(_db_error &e)
    {
        MLOG(0) << "Auth...db_error: " << e.error_st() << "; sid: 1" << endl;

        if (tr)
        {
            tr->commit();
            delete tr;
        }

        hh->http_file(_root_dir,"/auth_err.html",rez);
        hh->release();
        return;
    }

    delete db;

    list<std::string> params;
    list<std::string> values;
    hh->req_params(req,params,values);
    MLOG(0) << " Login - Params parse done; sid: 1" << endl;

    if (hh->req_get_param(params,values,"act")=="auth_do")
    {
        req="GET "+hh->req_get_param(params,values,"do")+" HTTP/1.1\r\n";
        params.clear();
        values.clear();
        hh->req_params(req,params,values);
        MLOG(0) << " Login - DO! params parse done; sid: 1" << endl;
    }

    MLOG(0) << " Login - make new session; sid: 1" << endl;
    _client_session *cs=new_session(params,values,user_id.value(),0,user_groups);
    cs->set_old_req(req);
    cs->cmd(params,values,rez);

/*
    list<std::string> params;
    list<std::string> values;
    hh->req_params(req,params,values);

    user_groups.push_back("admin");

    _client_session *cs=new_session(params,values,1,0,user_groups);
    cs->set_old_req(req);
    cs->cmd(params,values,rez);
*/
}

// -------------------------------------------------------------
_client_session *_session_manager::new_session(list<std::string> &params,list<std::string> &values,int user_id,int parent_id,list<std::string> &user_groups)
{
    // del old & unuseble sessions
    list<_client_session *>::iterator it=slist.begin();
    for(;it!=slist.end();it++)
        if (difftime(time(NULL),(*it)->last_need())>60*60)  // 1 hour
        {
            char tmp[128];
            sprintf(tmp,"Delete unused session. sid=%i",(*it)->id());
            MLOG(0) << tmp << "; sid: 1" << endl;
            delete (*it);
            slist.erase(it);
            it=slist.begin();
        }

    _http_help *hh=create_http_help();

    _client_session *cs=new _client_session;
    cs->set_root_dir(_root_dir);
    cs->set_parent_id(parent_id);
    cs->set_user_id(user_id);
    cs->set_user_groups(user_groups);

    slist.push_back(cs);

    char tmp[128];
    sprintf(tmp,"%i",cs->id());
    params.push_back("sid"); values.push_back(tmp);
    params.push_back("srnd"); values.push_back("0");

    hh->release();

    sprintf(tmp,"New session make. sid=%i",cs->id());
    MLOG(0) << tmp << "; sid: 1" << endl;

    return cs;
}

// --------------------------------------
// --------------------------------------

// --------------------------------------
void _http_db_error::out(const std::string &root_dir,const std::string &err_file,const std::string &err_text,std::string &rez)
{
    _http_help *hh=create_http_help();
    _report *hr=create_html_report();

    hh->http_file(root_dir,err_file,rez);
    hr->setTemplateText(rez.c_str());
    hr->setVarNoRep("ERROR_TEXT",err_text);
    hr->outSection("MAIN",rez);

    hr->release();
    hh->release();

    MLOG(0) << "get db - error : " << err_text << "; sid: 1" << endl;
}

