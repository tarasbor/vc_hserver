#ifndef __HTTP_CLIENT_SESSION__
#define __HTTP_CLIENT_SESSION__

#include <string>
#include <list>
#include <net_server.h>

using namespace std;

class _client_session;

class _client_object
{
    private:

    protected:

      std::string _class_name;
    _client_session *_session;

    public:

      _client_object(_client_session *session,const std::string &class_name):_class_name(class_name),_session(session) {};
      virtual ~_client_object() {};

      virtual const std::string &class_name() const { return _class_name; };

      virtual int cmd(std::string &act,list<std::string> &params,list<std::string> &values,std::string &rez)=0;

};

// ----------------------------------------------------------------
class _client_session
{
    private:

      int _id;
      int _last_need;
      int _rnd;
      int _parent_id;
      std::string _root_dir;
      _client_object *obj;
      std::string _old_req;
      int _user_id;
      list<std::string> _user_groups;

      list<std::string> ret_fields;

    protected:
    public:

      bool operator==(const _client_session &s) const { return _id==s._id; };
      bool operator<(const _client_session &s) const { return _last_need<s._last_need; };

      inline int id() { return _id; };
      inline int rnd() { return _rnd; };
      inline int last_need() { return _last_need; };
      inline int parent_id() { return _parent_id; };

      inline void set_root_dir(std::string &s) { _root_dir=s; };
      inline const std::string &root_dir() const { return _root_dir; };
      inline const std::string &old_req() const { return _old_req; };
      inline int user_id() const { return _user_id; };
      inline list<std::string> &user_groups() { return _user_groups; };

      inline void set_old_req(std::string &req) { _old_req=req; };
      inline void set_parent_id(int parent_id) { _parent_id=parent_id; };
      inline void set_user_id(int user_id) { _user_id=user_id; };
      inline void set_user_groups(const list<std::string> &user_groups) { _user_groups=user_groups; };

      void cmd(list<std::string> &params,list<std::string> &values,std::string &rez);
      void access_cut(std::string &rez);
      void add_sid_rid(std::string &rez);

      void save_return_fields(const std::string &line);

      _client_session();
      ~_client_session();
};

// ----------------------------------------------------------------
/*struct _lib_object_class
{
    public:

      std::string name;
      int reference;

      bool operator<(const _lib_object_class &o) const { return name<o.name; };
      bool operator==(const _lib_object_class &o) const { return name==o.name; };

      _lib_object_class():name(""),reference(0) {};

      _lib_object_class(const _lib_object_class &o):name(o.name),reference(o.reference) {};

      typedef void (*get_names_f)(list<std::string> &names);

    private:

};*/

// ----------------------------------------------------------------
class _session_manager:public _net_server_processor
{
    private:

      list<_client_session *> slist;
      //list<_lib_object_class> objs_lib;

      std::string _root_dir;
      std::string _lib_dir;

      virtual bool process(std::string &in,std::string &out,_net_server_connection *ns);

      void auth(std::string &req,std::string &rez,std::string &uname,std::string &upass);
      _client_session *new_session(list<std::string> &params,list<std::string> &values,int user_id,int parent_id,list<std::string> &user_groups);

    protected:
    public:

      _session_manager(const char *root_dir,const char *lib_dir);
      virtual ~_session_manager();

      virtual void reg_connection(_net_server_connection *c) {};
      virtual void unreg_connection(_net_server_connection *c) {};
};

// ----------------------------------------------------
class _http_db_error
{
    public:

      static void out(const std::string &root_dir,const std::string &err_file,const std::string &err_text,std::string &rez);
};

#endif
