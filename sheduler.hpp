#ifndef __SHEDULER_H__
#define __SHEDULER_H__

#include <clthread.hpp>
#include <string>

class _sheduler: public _thread
{            
    public:

      class _task
      {
          private:
                      
            int sec_interval;
            std::string _name;
            
          public:

            virtual bool make_actions() = 0;

            _task(int interval,const std::string &name):sec_interval(interval),_name(name) {};
            virtual ~_task() {};

            int interval() const { return sec_interval; };
            void interval(int i) { sec_interval=i; };
            const std::string &name() const { return _name; };
            
      };

      _sheduler() {};
      virtual ~_sheduler() {};

      virtual void add_task(_task *t) = 0;
      virtual void remove_task(const std::string &name) = 0;
   
};

// --------------------------------------------------
_sheduler *create_sheduler();

#endif
