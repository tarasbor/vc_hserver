#ifndef __FROM_H__
#define __FROM_H__

#include "sheduler.hpp"
#include <xml/ut_xml_par.hpp>
#include <xml/ut_xml_tag.hpp>
#include <obj_db\base.hpp>
#include "session.hpp"
#include "obj/user.hpp"

class _import: public _sheduler::_task
{
    private:

      std::string _path;
      std::string _template_path;
      _session_manager *s_manager;
      int user_id;
      
      
      bool make_one(const std::string &fname);

      bool reg_candidate(ut_xml_par *parser);
      bool in_candidate(ut_xml_par *parser);
      bool out_candidate(ut_xml_par *parser);
      bool move_job_person(ut_xml_par *parser);
      bool dismiss_job_person(ut_xml_par *parser);

      int find_or_make_post(_transaction &tr,const std::string &post);
      int find_or_make_wplace(_transaction &tr,const std::string &wplace,int parent_id=0);
      
    public:

      virtual bool make_actions();

      _import(int interval,const std::string &path,_session_manager *sm,const std::string &template_path);
      virtual ~_import();

};

#endif
