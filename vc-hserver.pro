#-Wl,--subsystem,windows -mthreads

TEMPLATE = app
#CONFIG += console
#CONFIG += windows
CONFIG -= app_bundle
CONFIG -= qt

TARGET = hserver

INCLUDEPATH += ../../libcommon
INCLUDEPATH += ../../libcommon/system
INCLUDEPATH += ../../libcommon/log2
INCLUDEPATH += ../../libcommon/obj_db5
INCLUDEPATH += ../../libcommon/otl4
INCLUDEPATH += ../../libcommon/thread
INCLUDEPATH += ../../libcommon/htmlreport
INCLUDEPATH += ../../libcommon/xml
INCLUDEPATH += ../../libcommon/xml/lib
INCLUDEPATH += ../../libcommon/xml/bin
INCLUDEPATH += ../../libcommon/net
#INCLUDEPATH += ../../libcommon/com
INCLUDEPATH += ./http_obj
INCLUDEPATH += ../common/db_obj
#INCLUDEPATH += ../oci/include
INCLUDEPATH += /opt/ibm/db2/V9.7/include

#launch under linux,db2
DEFINES += VC_DB2

CONFIG(windows) {
    DEFINES *= WINDOWS
}
DEFINES -= UNICODE
DEFINES *= DB_USE_COMMON_TIME

CONFIG(debug, debug|release) {
    DEFINES += RUN_TEST
#    DEFINES += NET_LIB_LOG
    DEFINES *= KEEP_ALIVE_TIME
    DEFINES += AS_APP
    DEFINES -= AS_SERVICE
    message("--- debug ---")
}
else {
    DEFINES -= AS_APP
    DEFINES += AS_SERVICE
    message("--- release ---")
}

SOURCES += \
    ../../libcommon/log2/log_file_writer.cpp \
    ../../libcommon/log2/log.cpp \
    ../../libcommon/system/error.cpp \
    ../../libcommon/system/sheduler.cpp \
    ../../libcommon/system/codepage.cpp \
    ../../libcommon/system/system_env.cpp \
    ../../libcommon/system/text_help.cpp \
    ../../libcommon/system/time.cpp \
    ../../libcommon/thread/clthread.cpp \
    ../../libcommon/net/net_http_server.cpp \
    ../../libcommon/net/net_http.cpp \
    ../../libcommon/net/net_server_tcp.cpp \
    ../../libcommon/net/net_lib_error.cpp \
    ../../libcommon/net/net_line_tcp.cpp \
    ../../libcommon/obj_db5/base.cpp \
    ../../libcommon/xml/lib/ut_xml_par.cpp \
    ../../libcommon/xml/lib/ut_xml_tag.cpp \
    ../../libcommon/htmlreport/lib/htmlrep.cpp \
    ../common/settings.cpp \
    ../common/net_control_client.cpp \
    ../common/db_obj/workstation.cpp \
    ../common/db_obj/user.cpp \
    ../common/db_obj/terminal.cpp \
    ../common/db_obj/server.cpp \
    ../common/db_obj/constant.cpp \
    http_obj/http_constant.cpp \
    http_obj/http_server.cpp \
    http_obj/http_terminal.cpp \
    http_obj/http_workstation.cpp \
    http_obj/http_user.cpp \
    session.cpp \
    main.cpp \
    gl_hserver.cpp \
    ../common/db_obj/queue.cpp \
    http_obj/http_report.cpp \
    ../common/db_obj/storage.cpp \
    ../../libcommon/system/config_parser.cpp \
    http_obj/http_calendar.cpp \
    ../../libcommon/obj_db5/base_db2.cpp \
    ../common/db_obj/work_time.cpp \
    ../../libcommon/net/net_cmd_client.cpp \
    ../../libcommon/net/net_lib.cpp \
    ../../libcommon/system/file_utils.cpp

HEADERS += \
    ../../libcommon/log2/log_file_writer.hpp \
    ../../libcommon/log2/log.hpp \
    ../../libcommon/system/codepage.hpp \
    ../../libcommon/system/error.hpp \
    ../../libcommon/system/sheduler.hpp \
    ../../libcommon/system/system_env.hpp \
    ../../libcommon/system/text_help.hpp \
    ../../libcommon/system/time.hpp \
    ../../libcommon/net/net_server.h \
    ../../libcommon/net/net_http_server.h \
    ../../libcommon/net/net_http.h \
    ../../libcommon/net/net_server_tcp.h \
    ../../libcommon/net/net_lib_error.h \
    ../../libcommon/net/net_line_tcp.h \
    ../../libcommon/net/net_lib.h \
    ../../libcommon/net/net_line.h \
    ../../libcommon/obj_db5/base.hpp \
    ../../libcommon/otl4/otlv4.h \
    ../../libcommon/thread/clthread.hpp \
    ../../libcommon/xml/ut_xml_tag.hpp \
    ../../libcommon/xml/ut_xml_par.hpp \
    ../../libcommon/htmlreport/htmlrep.hpp \
    ../common/version.hpp \
    ../common/settings.hpp \
    ../common/net_control_client.hpp \
    ../common/db_obj/workstation.hpp \
    ../common/db_obj/user.hpp \
    ../common/db_obj/terminal.hpp \
    ../common/db_obj/server.hpp \
    ../common/db_obj/constant.hpp \
    http_obj/http_constant.hpp \
    http_obj/http_server.hpp \
    http_obj/http_terminal.hpp \
    http_obj/http_workstation.hpp \
    http_obj/http_user.hpp \
    session.hpp \
    gl_hserver.hpp \
    ../common/db_obj/queue.hpp \
    http_obj/http_report.hpp \
    ../common/db_obj/storage.hpp \
    ../../libcommon/system/config_parser.hpp \
    http_obj/http_calendar.hpp \
    ../../libcommon/obj_db5/base_db2.hpp \
    ../common/db_obj/work_time.hpp \
    ../../libcommon/net/net_cmd_client.hpp \
    ../../libcommon/system/file_utils.hpp \
    ../../libcommon/system/udefs.hpp

win32: LIBS += C:/DevTools/Qt/5.0.2/Tools/MinGW/i686-w64-mingw32/lib/libws2_32.a \
               D:/projects/vc/oci/lib/liboci.a

#win64: LIBS += C:/DevTools/Qt/5.0.2/Tools/MinGW/i686-w64-mingw32/lib64/libws2_32.a \
#                D:/projects/vc/oci/lib/liboci64.a

unix: LIBS += /lib/librt.so.1 \
              /lib/libpthread.so.0
#              /opt/oracle/product/11.2.0/client_1/lib/libclntsh.so.11.1
unix:LIBS += /opt/ibm/db2/V9.7/lib32/libdb2.so

#win32: LIBS += -luser32 -lshell32 -lgdi32 -lole32 -ladvapi32 -lodbc32
#release:LIBS += ../../libcommon/htmlreport/lib/htmlrep_lib.a
#debug:LIBS += ../../libcommon/htmlreport/lib/htmlrep_libd.a

OTHER_FILES += \
    hserver.set \
    html/adm/menu.html \
    html/adm/index.html \
    html/comctl/yesno.html \
    html/comctl/send_done.html \
    html/comctl/send.html \
    html/comctl/return.html \
    html/comctl/remove.html \
    html/comctl/img_select_done.html \
    html/comctl/img_holder.html \
    html/comctl/img.html \
    html/comctl/ex.html \
    html/comctl/error.html \
    html/comctl/dup.html \
    html/comctl/done.html \
    html/comctl/document.html \
    html/comctl/doc_is_open.html \
    html/comctl/date.html \
    html/comctl/calendar.html \
    html/comctl/adress.html \
    html/constant/constants_list.html \
    html/constant/constant.html \
    html/err/message.html \
    html/err/common.html \
    html/err/bad_img.JPG \
    html/help/up_begin.html \
    html/help/search.html \
    html/help/personal.html \
    html/help/instruction_ukr.html \
    html/help/help_index.html \
    html/help/doc_state.html \
    html/help/begin_1.jpg \
    html/help/begin.html \
    html/help/about.html \
    html/img/vline_24.png \
    html/img/vline.png \
    html/img/vcard.png \
    html/img/users_del_48.png \
    html/img/users_del.png \
    html/img/users_add_48.png \
    html/img/users_add.png \
    html/img/users_48.png \
    html/img/users.png \
    html/img/user_del_48.png \
    html/img/user_del.png \
    html/img/user_add_48.png \
    html/img/user_add.png \
    html/img/user_48.png \
    html/img/user.png \
    html/img/up.JPG \
    html/img/unbind_key.bmp \
    html/img/tools_48.png \
    html/img/to_bag.ico \
    html/img/to_bag.gif \
    html/img/to_bag.bmp \
    html/img/show_object2.gif \
    html/img/show_object.gif \
    html/img/send_mail.bmp \
    html/img/select.JPG \
    html/img/print_back.jpg \
    html/img/print.png \
    html/img/presentation.png \
    html/img/no_object.gif \
    html/img/no.jpg \
    html/img/new_object_down.gif \
    html/img/new_object.gif \
    html/img/move_up.png \
    html/img/move_down.png \
    html/img/look_in_48.png \
    html/img/look_in.png \
    html/img/key_del.png \
    html/img/key_add.png \
    html/img/key.png \
    html/img/insert_up.png \
    html/img/insert_down.png \
    html/img/info_48.png \
    html/img/in.JPG \
    html/img/help_48.png \
    html/img/gear_48.png \
    html/img/edit_object.gif \
    html/img/close_48.png \
    html/img/chart.png \
    html/img/card_unbind_48.png \
    html/img/card_from_reader_48.png \
    html/img/card_del_48.png \
    html/img/card_del.png \
    html/img/card_bind_48.png \
    html/img/card_add_from_reader_48.png \
    html/img/card_add_48.png \
    html/img/card_add.png \
    html/img/card_48.png \
    html/img/card.png \
    html/img/bases_48.png \
    html/img/back_form.jpg \
    html/img/back.jpg \
    html/img/aleft_48.png \
    html/reports/report_in_out_table_one_week.html \
    html/reports/report_in_out_table_one_day.html \
    html/reports/report_in_out_table_export_one_day.html \
    html/reports/report_in_out_table.html \
    html/reports/report_event_table.html \
    html/reports/print_card.html \
    html/reports/menu.html \
    html/reports/index.html \
    html/reports/1.xml \
    html/reports/1.xls \
    html/select/sel.html \
    html/select/main.html \
    html/server/servers_list.html \
    html/server/queues.html \
    html/server/queues_list.html \
    html/server/qserver.html \
    html/server/mserver.html \
    html/server/hserver.html \
    html/ssi/up_print.html \
    html/ssi/up.html \
    html/ssi/main_menu.html \
    html/ssi/down.html \
    html/terminal/terminals_list.html \
    html/terminal/terminal.html \
    html/user/user.html \
    html/user/list.html \
    html/user/group.html \
    html/user/curr_user.html \
    html/workstation/workstations_list.html \
    html/workstation/workstation.html \
    html/style.css \
    html/session_err.html \
    html/news.html \
    html/index.html \
    html/hello_no_help.html \
    html/hello_help.html \
    html/hello.html \
    html/command_err.html \
    html/auth_err.html \
    html/auth_do.html \
    ../common/imp.def \
    ../common/global.set \
    ../common/builds.txt \
    ../common/auth_frm.rc \
    hserver.conf \
    html/reports/report_full.html \
    html/server/storages_list.html \
    html/server/storage.html \
    html/server/queue.html \
    vc_hserver.conf \
    html/reports/report_full_export.html \
    html/constant/delete_constant.html \
    html/reports/report_by_hours_export.html \
    html/reports/report_by_hours.html \
    html/img/delete.png \
    html/user/permission.html \
    html/user/delete_permission.html
